<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/base.jsp" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/favicon.ico">
    <title>珠江-西江经济带发展研究院</title>
    <link href="/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/dist/css/main.css">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://apps.bdimg.com/libs/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/dist/js/main.js"></script>
</head>
<body>
<div id="header">
    <div class="container">
        <div class="head-content">
            <div class="en_link">
                <div class="link-item">
                    <a href="">
                        <span class="item-name">English</span>
                    </a>
                </div>
            </div>
            <div class="search-panel">
                <div class="search-window">
                    <div class="wp-search clearfix" style="height: 37px">
                        <form>
                            <div class="search-input">
                                <input class="search-title" name="keyword" type="text" placeholder="Search...">
                            </div>
                            <div class="search-btn">
                                <input class="search-submit" type="button">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="nav">
    <div class="container">
        <div class="nav-content">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="to-hover col-md-4">
                            <a href="/index">首页</a>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                院情介绍 <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/static/web/introduce/1.jsp">院情概况</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/introduce/2.jsp">发展目标</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/introduce/3.jsp">研究方向</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/introduce/4.jsp">联系方式</a>
                                </div>
                            </div>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                机构设置 <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/static/web/organization/1.jsp">学术委员会</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/organization/2.jsp">组织架构</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/organization/3.jsp">研究中心</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                学术动态
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/science/1">新闻速递</a>
                                </div>
                                <div class="item">
                                    <a href="/science/2">学术活动</a>
                                </div>
                                <div class="item">
                                    <a href="/science/3">通知公告</a>
                                </div>
                            </div>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                科学研究
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/static/web/research/1.jsp">科研团队</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/research/2.jsp">科研项目</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/research/3.jsp">研究成果</a>
                                </div>
                            </div>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="/static/web/consult/1.jsp">咨询服务
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/static/web/consult/1.jsp">横向课题</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/consult/2.jsp">调研工作</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/consult/3.jsp">最近项目</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                人才培养
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/static/web/personnel/1.jsp">培养方案</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/personnel/2.jsp">培养成果</a>
                                </div>
                            </div>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                信息平台
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/static/web/information/1.jsp">数据库平台</a>
                                </div>
                                <div class="item">
                                    <a href="/static/web/information/2.jsp">中英文期刊</a>
                                </div>
                            </div>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                智库系列
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/library">信息简报</a>
                                </div>
                                <div class="item">
                                    <a href="/library/1">智库专刊</a>
                                </div>
                                <div class="item">
                                    <a href="/library/2">蓝皮书</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

