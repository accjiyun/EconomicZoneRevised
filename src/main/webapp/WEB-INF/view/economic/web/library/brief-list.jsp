<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp" %>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="/index">首页</a>
            >
            <a href="/library">智库系列</a>
            >
            <a href="/library/4">信息简报</a>

        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>智库系列</h2>
                    <ul class="topNav">
                        <li class="a">
                            <a class="as" href="/library/4">信息简报</a>

                        </li>
                        <li class="a">
                            <a class="as" href="/library/1">智库专刊</a>
                        </li>
                        <li class="a">
                            <a class="as" href="/library/2">蓝皮书</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>信息简报</h2>
                    <div class="textHer">
                        <ul class="infoCenter">
                            <c:if test="${not empty articleList}">
                                <c:forEach var="article" items="${articleList}">
                                    <li>
                                        <a href="/library/briefinfo/${article.articleId}.html">【<span
                                                class="library-type-convert"
                                                id="${article.articleType}"></span>】${article.title}</a>
                                        <b><fmt:formatDate value="${article.publishTime }" pattern="yyyy年MM月"/></b>
                                    </li>
                                </c:forEach>
                            </c:if>
                            <c:if test="${empty articleList}">
                                待更新...
                            </c:if>
                        </ul>
                        <!-- 公共分页 开始 -->
                        <form action="/library/${articleType}" method="post" id="searchForm">
                            <input type="hidden" name="page.pageNo" id="pageCurrentPage" value="1">
                        </form>
                        <div>
                            <jsp:include page="/WEB-INF/view/common/front_page.jsp"></jsp:include>
                            <div class="clear"></div>
                        </div>
                        <!-- 公共分页 结束 -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp" %>