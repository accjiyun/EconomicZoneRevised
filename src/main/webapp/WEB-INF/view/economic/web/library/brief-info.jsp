<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp" %>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="/index">首页</a>
            >
            <a href="/library">智库系列</a>
            >
            <a href="/library/4">信息简报</a>
            >
            <a href="#"><fmt:formatDate value="${article.publishTime }" pattern="yyyy年MM月"/></a>
        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>智库系列</h2>
                    <ul class="topNav">
                        <li class="a">
                            <a class="as" href="/library/4">信息简报</a>

                        </li>
                        <li class="a">
                            <a class="as" href="/library/1">智库专刊</a>
                        </li>
                        <li class="a">
                            <a class="as" href="/library/2">蓝皮书</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2><fmt:formatDate value="${article.publishTime }"
                                        pattern="yyyy年MM月"/></h2>
                    <div class="textHer">
                        <h3>${article.title}</h3>
                        ${content}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp" %>