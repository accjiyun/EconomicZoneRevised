<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/top.jsp" %>
<div id="pr" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <c:if test="${not empty bannerList }">
            <c:forEach var="banner" items="${bannerList }" varStatus="status">
                <li data-target="#carousel-example-generic" data-slide-to="${status.count - 1}"<c:if
                        test="${status.count == 1}"> class="active"</c:if>></li>
            </c:forEach>
        </c:if>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <c:if test="${not empty bannerList }">
            <c:forEach var="banner" items="${bannerList }" varStatus="status">
                <div class="item<c:if test="${status.count == 1}"> active</c:if>"
                     style="background-image: url(${banner.imageUrl})">
                    <a class="img-link" href="${banner.articleUrl}"></a>
                    <div class="carousel-caption">${banner.title}</div>
                </div>
            </c:forEach>
        </c:if>
    </div>


    <!-- Controls -->
    <a class="left carousel-control" href="#pr" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#pr" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div id="content">
    <div class="container">
        <div id="part1">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="part-content">
                            <h2 class="yahei">
                                <a href="/science/1" class="more" target="_blank">更多 &gt;</a>
                                <div class="head-title">新闻速递</div>
                            </h2>
                            <ul>
                                <c:if test="${not empty newsList }">
                                    <c:forEach var="news" items="${newsList }">
                                        <li class="clearfix">
                                            <div style="display: inline-block;float: left;">
                                                <img src="${news.imageUrl}" height="58px" width="58px" alt="">
                                            </div>
                                            <div class="tn">
                                                <h3>
                                                    <a href="/science/articleinfo/${news.articleId}.html"
                                                       target="_blank">${news.title}</a>
                                                </h3>
                                            </div>
                                        </li>
                                    </c:forEach>
                                </c:if>

                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="part-content">
                            <h2 class="yahei">
                                <a href="/science/3" class="more" target="_blank">更多 &gt;</a>
                                <div class="head-title">通知公告</div>
                            </h2>
                            <ul>
                                <c:if test="${not empty noticesList }">
                                    <c:forEach var="notice" items="${noticesList}">
                                        <li class="clearfix">
                                            <div class="date-wrapper">
                                                <div class="date-days">
                                                    <span class="date-display-single">
                                                        <fmt:formatDate value="${notice.publishTime }" pattern="dd"/>
                                                    </span>
                                                </div>
                                                <div class="date-months">
                                                    <span class="date-display-single mon-convert"
                                                          id="${notice.publishTime }"></span>
                                                </div>
                                            </div>
                                            <div class="tn">
                                                <h3>
                                                    <a href="/science/articleinfo/${notice.articleId}.html"
                                                       target="_blank">${notice.title}</a>
                                                </h3>
                                            </div>
                                        </li>
                                    </c:forEach>
                                </c:if>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="part-content zhannue">
                            <h2 class="yahei">
                                <div class="head-title">战略定位</div>
                            </h2>
                            <img style="width: 292px;height: 316px;margin-top: -15px;margin-left: 40px;"
                                 src="${websiteLogo.imageUrl}" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="part2">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="part-content">
                            <h2 class="yahei">
                                <a href="/science/2" class="more" target="_blank">更多 &gt;</a>
                                <div class="head-title">学术活动</div>
                            </h2>
                            <ul>
                                <c:if test="${not empty activitiesList }">
                                    <c:forEach var="activity" items="${activitiesList }">
                                        <li class="clearfix">
                                            <div style="display: inline-block;float: left;">
                                                <img src="${activity.imageUrl}" height="58px" width="58px" alt="">
                                            </div>
                                            <div class="tn">
                                                <h3>
                                                    <span style="font-size: 16px"></span>
                                                    <a href="/science/articleinfo/${activity.articleId}.html"
                                                       target="_blank">${activity.title}</a>
                                                </h3>
                                            </div>
                                        </li>
                                    </c:forEach>
                                </c:if>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="part-content">
                            <h2 class="yahei">
                                <a href="/library/1" class="more" target="_blank">更多 &gt;</a>
                                <div class="head-title">智库专刊</div>
                            </h2>
                            <div class="can-left">
                                <a href="/library/library/${library.libraryId}.html">
                                    <img style="box-shadow: 6px 6px 3px #888888;border-top: #f1eeee 1px solid;border-left: #f1eeee 1px solid;max-width: 170px;max-height: 270px;"
                                         src="${library.imageUrl}" alt=""></a>
                            </div>
                            <div class="can-right">
                                <div class="can-ever">
                                    <a href="/library/library/${library.libraryId}.html">
                                        ${libraryText}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="link">
    <div class="container">
        <div class="link-content">
            <div class="pic_link_tit">常用链接</div>
            <div class="row">
                <div class="col-md-4">
                    <a href="#">
                        <img src="/images/upload/links/link1.jpg" alt=""></a>
                    <a href="#">
                        <img src="/images/upload/links/link2.jpg" alt=""></a>
                </div>

                <div class="col-md-4">
                    <a href="#">
                        <img src="/images/upload/links/link3.jpg" alt=""></a>
                    <a href="https://socialhistory.org">
                        <img src="/images/upload/links/link4.jpg" alt=""></a>
                </div>
                <div class="col-md-4">
                    <a href="#">
                        <img src="/images/upload/links/link5.jpg" alt=""></a>
                    <a href="#">
                        <img src="/images/upload/links/link6.jpg" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp" %>