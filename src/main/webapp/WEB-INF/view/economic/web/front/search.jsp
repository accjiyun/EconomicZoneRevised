<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp"%>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="/index">首页</a>
            >
            <a href="javascript:;">搜索</a>
        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>搜索结果</h2>
                    <div class="textHer">
                        <ul class="infoCenter">
                            <c:if test="${not empty articleList}">
                                <c:forEach var="article" items="${articleList}">
                                    <li>
                                        <a href="/science/articleinfo/${article.articleId}.html">【<span
                                                class="article-type-convert"
                                                id="${article.articleType}"></span>】${article.title}</a>
                                        <b><fmt:formatDate value="${article.publishTime }" pattern="yyyy年MM月"/></b>
                                    </li>
                                </c:forEach>
                            </c:if>
                            <c:if test="${empty articleList}">
                                找不到结果...
                            </c:if>
                        </ul>
                        <!-- 公共分页 开始 -->
                        <form action="/showSearchList/" method="post" id="searchForm">
                            <input type="hidden" name="page.pageNo" id="pageCurrentPage" value="1">
                            <input type="hidden" name="keyword" value="${keyword}">
                        </form>
                        <div>
                            <jsp:include page="/WEB-INF/view/common/front_page.jsp"></jsp:include>
                            <div class="clear"></div>
                        </div>
                        <!-- 公共分页 结束 -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp"%>