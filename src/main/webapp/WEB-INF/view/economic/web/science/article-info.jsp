<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp" %>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="/index">首页</a>
            >
            <a href="javascript:;">学术动态</a>
            >
            <a href="/science/${article.articleType}">
                <span class="article-type-convert" id="${article.articleType}"></span>
            </a>
            >
            <a href="#"><fmt:formatDate value="${article.publishTime }" pattern="yyyy年MM月dd日"/></a>
        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>
        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>学术动态</h2>
                    <ul class="topNav">
                        <li class="a" style="height:auto;">
                            <a class="as" href="/science/1">新闻速递</a>
                        </li>
                        <li class="a">
                            <a class="as" href="/science/2">学术活动</a>
                        </li>
                        <li class="a">
                            <a class="as" href="/science/3">通知公告</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2><fmt:formatDate value="${article.publishTime }" pattern="yyyy年MM月dd日"/></h2>
                    <div class="textHer">
                        <h3>${article.title}</h3>
                        <div>${content}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp" %>