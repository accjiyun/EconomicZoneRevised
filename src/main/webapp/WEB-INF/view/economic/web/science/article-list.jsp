<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp" %>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="/index">首页</a>
            >
            <span>学术动态</span>
            <c:if test="${articleType != 0}">
                >
                <span class="article-type-convert" id="${articleType}"></span>
            </c:if>
        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>学术动态</h2>
                    <ul class="topNav">
                        <li class="a" style="height:auto;">
                            <a class="as" href="/science/1">新闻速递</a>

                        </li>
                        <li class="a" style="height:auto;">
                            <a class="as" href="/science/2">学术活动</a>
                        </li>
                        <li class="a">
                            <a class="as" href="/science/3">通知公告</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>学术动态</h2>
                    <div class="textHer">
                        <ul class="infoCenter">
                            <c:if test="${not empty articleList}">
                                <c:forEach var="article" items="${articleList}">
                                    <li>
                                        <a href="/science/articleinfo/${article.articleId}.html">【<span
                                                class="article-type-convert"
                                                id="${article.articleType}"></span>】${article.title}</a>
                                        <b><fmt:formatDate value="${article.publishTime }" pattern="yyyy年MM月"/></b>
                                    </li>
                                </c:forEach>
                            </c:if>
                            <c:if test="${empty articleList}">
                                暂无记录
                            </c:if>
                        </ul>
                        <!-- 公共分页 开始 -->
                        <form action="/science/${articleType}" method="post" id="searchForm">
                            <input type="hidden" name="page.pageNo" id="pageCurrentPage" value="1">
                        </form>
                        <div>
                            <jsp:include page="/WEB-INF/view/common/front_page.jsp"></jsp:include>
                            <div class="clear"></div>
                        </div>
                        <!-- 公共分页 结束 -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp" %>