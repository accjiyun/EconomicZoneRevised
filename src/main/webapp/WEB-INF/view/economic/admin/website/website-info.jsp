<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<style>
    .site-demo-upload,
    .site-demo-upload img {
        width: 200px;
        height: 200px;
    }

    .site-demo-upload {
        position: relative;
        clear: both;
        display: block;
        margin: auto;
    }

    .site-demo-upload .site-demo-upbar {
        position: absolute;
        top: 50%;
        left: 50%;
        margin: -18px 0 0 -56px;
    }
</style>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding" style="text-align: center;">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 50px;">
                    <legend>上传预览图</legend>
                </fieldset>
                <div class="layui-form-item">
                    <div class="site-demo-upload">
                        <img id="showImage" name="infoImg" src="${imageUrl}">
                        <div class="site-demo-upbar">
                            <input type="file" name="file" lay-ext="jpg|png|gif" class="layui-upload-file">
                        </div>
                    </div>
                </div>
                <form id="form1" class="layui-form layui-form-pane" action="/admin/websiteInfo/updateWebsiteInfo">
                    <input type="hidden" id="imageUrl" name="imageUrl">
                    <div class="layui-form-item">
                        <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('websiteInfoForm');

    function callback(imgUrl) {
        $("input[name='imageUrl']").val(imgUrl);
        $("#showImage").attr("src", imgUrl)
    }
</script>
</html>