<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding">
                <form id="form1" class="layui-form layui-form-pane" data-name="articleData" data-tpl="list-tpl"
                      data-render="true" action="/admin/websiteBanner/createBanner">
                    <div class="layui-form-item">
                        <label class="layui-form-label">文章标题</label>
                        <div class="layui-input-block">
                            <input type="text" name="title" required jq-verify="" jq-error="请输入标题|请输入数字"
                                   placeholder="请输入标题" autocomplete="off" class="layui-input ">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">跳转URL</label>
                        <div class="layui-input-block">
                            <input type="text" name="articleUrl" required jq-verify="required" jq-error="请输入跳转URL"
                                   placeholder="请输入跳转URL" autocomplete="off" class="layui-input ">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">上传轮播图</label>
                        <div class="layui-input-block">
                            <input type="file" name="file" class="layui-upload-file" id="banner">
                            <input type="hidden" name="imageUrl" error-id="img-error">
                            <p id="img-error" class="error"></p>
                        </div>
                        <div class="layui-input-block">
                            <div class="imgbox" style="height: 100px;width: 500px;">
                                <img name="imageUrl" src="/images/imgs/article-bg.jpg" alt="暂无" class="img-thumbnail">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item ">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-inline">
                            <input type="text" name="sort" value="0" jq-verify="number" jq-error="只能填写数字" autocomplete="off"
                                   class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" jq-submit jq-filter="submit">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('myform');
</script>

</html>