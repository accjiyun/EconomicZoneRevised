<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">
    <!--顶部统计数据预览 -->
    <div class="row">

        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-orange"><i class="iconfont">&#xe653;</i>
                </div>
                <div class="value tab-menu" style="margin-top: 2em">
                    <a href="javascript:;" data-url="/admin/article/initAddArticle"
                       data-parent="true" data-title="添加文章"> <i class="iconfont" data-icon='&#xe653;'></i>
                        <span style="font-size: 19px;">添加文章</span>
                    </a>
                </div>
            </section>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-dark-green"><i class="iconfont">&#xe64a;</i>
                </div>
                <div class="value tab-menu">
                    <a href="javascript:;" data-url="/admin/article/initArticleList"
                       data-parent="true" data-title="文章列表"> <i class="iconfont " data-icon='&#xe64a;'></i>
                        <h1>${articleNum}</h1>
                    </a>
                    <a href="javascript:;" data-url="/admin/article/initArticleList"
                       data-parent="true" data-title="文章列表"> <i class="iconfont "
                                                                data-icon='&#xe64a;'></i><span>文章列表</span></a>
                </div>
            </section>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-yellow"><i class="iconfont">&#xe61c;</i>
                </div>
                <div class="value tab-menu" style="margin-top: 2em">
                    <a href="javascript:;" data-url="/admin/library/initAddLibrary"
                       data-parent="true" data-title="添加文献"> <i class="iconfont " data-icon='&#xe61c;'></i>
                        <span style="font-size: 19px;">添加文献</span>
                    </a>
                </div>
            </section>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-blue"><i class="iconfont">&#xe634;</i>
                </div>
                <div class="value tab-menu">
                    <a href="javascript:;" data-url="/admin/library/initLibraryList"
                       data-parent="true" data-title="文献列表"><i class="iconfont " data-icon='&#xe634;'></i>
                        <h1>${libraryNum}</h1>
                    </a>
                    <a href="javascript:;" data-url="/admin/library/initLibraryList"
                       data-parent="true" data-title="文献列表"> <i class="iconfont "
                                                                data-icon='&#xe634;'></i><span>文献列表</span></a>
                </div>
            </section>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-commred"><i class="iconfont">&#xe681;</i>
                </div>
                <div class="value tab-menu" style="margin-top: 2em">
                    <a href="javascript:;" data-url="/admin/user/initUserUpdatePage"
                       data-parent="true" data-title="账号管理"> <i class="iconfont " data-icon='&#xe672;'></i>
                        <span style="font-size: 19px;">账号管理</span>
                    </a>
                </div>
            </section>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-yellow-green"><i class="iconfont">&#xe654;</i>
                </div>
                <div class="value tab-menu" style="margin-top: 2em">
                    <a href="javascript:;" data-url="/admin/websiteBanner/initBannerList"
                       data-parent="true" data-title="首页轮播"> <i class="iconfont " data-icon='&#xe654;'></i>
                        <span style="font-size: 19px;">首页轮播</span>
                    </a>
                </div>
            </section>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-12">
            <section class="panel">
                <div class="panel-heading">
                    后台登录记录
                    <a href="javascript:;" class="pull-right panel-toggle"><i class="iconfont">&#xe604;</i></a>
                </div>
                <div class="panel-body" style="text-align: center;">
                    <div id="list" class="layui-form"></div>
                    <div class="text-right" id="page"></div>
                </div>
            </section>
        </div>
    </div>

</div>
</body>
<%@include file="/WEB-INF/layouts/admin/loginLog.jsp" %>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use(['main']);
    layui.use('list');
</script>

</html>