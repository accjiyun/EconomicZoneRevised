<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <!--头部搜索-->
            <section class="panel panel-padding">
                <form class="layui-form" action="/admin/library/getLibraryList?pageNo=1">
                    <div class="layui-form">
                        <div class="layui-inline">
                            <select name="libraryType" jq-verify="required" jq-error="请输入文献类型" lay-filter="verify">
                                <option value="0">请选择类型</option>
                                <option value="1">智库专刊</option>
                                <option value="2">蓝皮书</option>
                            </select>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input start-date" name="start_date" placeholder="注册时间">
                            </div>
                            <div class="layui-input-inline">
                                <input class="layui-input end-date" name="end_date" placeholder="注册时间">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input" name="keyword" placeholder="关键字">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button lay-submit class="layui-btn" lay-filter="search">查找</button>
                        </div>
                    </div>
                </form>
            </section>

            <!--列表-->
            <section class="panel panel-padding">
                <div class="group-button">
                    <button class="layui-btn layui-btn-small layui-btn-danger ajax-all" data-name="libraryId"
                            data-params='{"url": "/admin/library/delete","loading":"true"}'>
                        <i class="iconfont">&#xe626;</i> 删除
                    </button>
                    <button class="layui-btn layui-btn-small modal-iframe"
                            data-params='{"content": "/admin/library/initAddLibrary", "title": "添加文章","full":"true"}'>
                        <i class="iconfont">&#xe649;</i> 添加
                    </button>
                </div>
                <div id="list" class="layui-form"></div>

                <div class="text-right" id="page"></div>
            </section>
        </div>
    </div>
</div>

<div class="add-subcat">
    <form id="form1" class="layui-form layui-form-pane" data-name="libraryData" data-tpl="list-tpl"
          data-render="true" action="/admin/library/update" method="post">

        <div class="layui-form-item">
            <label class="layui-form-label">文献标题</label>
            <div class="layui-input-block">
                <input type="text" name="title" required jq-verify="" jq-error="请输入标题|请输入数字" placeholder="请输入标题"
                       autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">上传预览图</label>
            <div class="layui-input-block">
                <input type="file" name="file" class="layui-upload-file" id="libraryView">
                <input type="hidden" name="imageUrl" error-id="img-error">
                <p id="img-error" class="error"></p>
            </div>
            <div class="layui-input-block">
                <div class="imgbox" style="min-height: 160px;">
                    <img name="imageUrl" src="/favicon.ico" alt="暂无" class="img-thumbnail">
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">上传文件</label>
            <div class="layui-input-block">
                <input type="file" name="file" lay-type="file" class="layui-upload-file" id="library">
                <input type="hidden" name="libraryUrl" error-id="img-error">
                <p id="file-error" class="error"></p>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">文献类型</label>
            <div class="layui-input-inline">
                <select name="libraryType" jq-verify="required" jq-error="请输入文献类型" lay-filter="verify">
                    <option value="0">请选择类型</option>
                    <option value="1">智库专刊</option>
                    <option value="2">蓝皮书</option>
                </select>
            </div>
        </div>

        <label class="layui-form-label">文献摘要</label>
        <div class="layui-form-item layui-form-text">
            <div class="layui-input-block">
                <textarea name="summary" jq-verify="content" id="content" style="display:none;"></textarea>
            </div>
        </div>

        <div class="layui-form-item" pane>
            <label class="layui-form-label">发布时间</label>
            <div class="layui-input-block">
                <input class="layui-input birth-date" name="publishTime" placeholder="发布时间">
            </div>
        </div>

        <div class="layui-form-item ">
            <label class="layui-form-label">下载次数</label>
            <div class="layui-input-inline">
                <input type="text" name="downloadNum" value="0" jq-verify="number" jq-error="只能填写数字" autocomplete="off"
                       class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>

</body>
<%@include file="/WEB-INF/layouts/admin/library.jsp" %>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('list');
    layui.use('myform');

    function test(ret, options, $) {
        console.log($);
        alert("这是自定义的ajax返回方法，可以用$哦");
    }
</script>

</html>