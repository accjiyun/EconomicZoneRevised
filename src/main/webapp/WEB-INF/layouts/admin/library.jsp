<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html" data-params='{"url":"/admin/library/getLibraryList?pageNo=1","pageid":"#page","dataName":"libraryData"}'>
    <table id="example" class="layui-table lay-even" data-name="libraryData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="libraryId" lay-filter="check" lay-skin="primary"></th>
            <th>缩略图</th>
            <th>标题
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="title" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="title"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>类型</th>
            <th>发布时间
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="publishTime" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="publishTime"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="libraryId" value="{{ item.libraryId}}" lay-skin="primary"></td>
            <td>
                <div class="img"><img src='{{#if (item.imageUrl==null){}}/favicon.ico {{#} }} {{#if (item.imageUrl!=null){}}{{item.imageUrl}} {{#} }}'
                                      alt="暂无" style="width: 68px;height: 68px;" class="img-thumbnail">
                </div>
            </td>
            <td>{{ item.title}}</td>
            <td>{{ getLibraryTypeName(item.libraryType) }}</td>
            <td>{{ item.publishTime}}</td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "编辑 ：{{item.title}}","key":"libraryId={{ item.libraryId }}","type":"1"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/library/delete?libraryId={{item.libraryId}}","loading":"true"}}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>