<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html" data-params='{"url":"/admin/websiteBanner/getList?pageNo=1","pageid":"#page","dataName":"bannerData"}'>
    <table id="example" class="layui-table lay-even" data-name="bannerData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="bannerId" lay-filter="check" lay-skin="primary"></th>
            <th>轮播图</th>
            <th>文章标题
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="title" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="title"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>排序
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="sort" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="sort"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr>
            <td><input type="checkbox" name="bannerId" value="{{ item.bannerId}}" lay-skin="primary"></td>
            <td style="width: 432px">
                <div class="img" style="max-width: 400px;width: 400px;">
                    <img src='{{#if (item.imageUrl==null){}}/favicon.ico {{#} }} {{#if (item.imageUrl!=null){}}{{item.imageUrl}} {{#} }}'
                                      alt="暂无" class="img-thumbnail">
                </div>
            </td>
            <td>{{ item.title}}</td>
            <td>{{ item.sort}}</td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "编辑 ：{{item.title}}","type":"1","key":"bannerId={{item.bannerId}}"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/websiteBanner/delete","data":"bannerId={{item.bannerId}}"}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button></td>
            </td>
        </tr>
        {{# }); }}
        </tbody>

    </table>
</script>