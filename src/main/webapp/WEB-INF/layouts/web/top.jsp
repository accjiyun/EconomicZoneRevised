<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="sitemesh"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/favicon.ico">
    <title>珠江-西江经济带发展研究院</title>
    <link href="/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/dist/css/main.css">
</head>
<body>
<div id="header">
    <div class="container">
        <div class="head-content">
            <div class="en_link">
                <div class="link-item">
                    <a href="">
                        <span class="item-name">English</span>
                    </a>
                </div>
            </div>
            <div class="search-panel">
                <div class="search-window">
                    <div class="wp-search clearfix">
                        <form>
                            <div class="search-input">
                                <input class="search-title" type="text" placeholder="Search..."></div>
                            <div class="search-btn">
                                <input class="search-submit" type="text"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="nav">
    <div class="container">
        <div class="nav-content">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="to-hover col-md-4">
                            <a href="/index.jsp">首页</a>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                院情介绍 <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/introduce/1.jsp">院情概况</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/introduce/2.jsp">发展目标</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/introduce/3.jsp">研究方向</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/introduce/4.jsp">联系方式</a>
                                </div>
                            </div>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                机构设置 <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/organization/1.jsp">学术委员会</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/organization/2.jsp">组织架构</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/organization/3.jsp">研究中心</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                学术动态
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/science/1.jsp">新闻速递</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/science/2.jsp">学术活动</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/science/3.jsp">通知公告</a>
                                </div>
                            </div>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                科学研究
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/research/1.jsp">科研团队</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/research/2.jsp">科研项目</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/research/3.jsp">研究成果</a>
                                </div>
                            </div>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="/WEB-INF/view/economic/consult/1.jsp">咨询服务
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/consult/1.jsp">横向课题</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/consult/2.jsp">调研工作</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/consult/3.jsp">最近项目</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                人才培养
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/personnel/1.jsp">培养方案</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/personnel/2.jsp">培养成果</a>
                                </div>
                            </div>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                信息平台
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/information/1.jsp">数据库平台</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/information/2.jsp">中英文期刊</a>
                                </div>
                            </div>
                        </div>
                        <div class="to-hover col-md-4">
                            <a href="javascript:;">
                                智库系列
                                <i class="triangle_down_black"></i>
                            </a>
                            <div class="nav-hidden">
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/library/1.jsp">信息简报</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/library/2.jsp">智库专刊</a>
                                </div>
                                <div class="item">
                                    <a href="/WEB-INF/view/economic/library/3.jsp">蓝皮书</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

