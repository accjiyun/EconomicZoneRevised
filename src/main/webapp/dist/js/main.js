$(function () {
    var a = $('#imgh').height();
    $('.pr').height(a);
});

window.onload = function(){
    var monAbb = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var nodeMon = document.getElementsByClassName("mon-convert");
    for (var i = 0; i < nodeMon.length; i++) {
        var time = nodeMon[i].getAttribute("id").substr(5, 2);
        nodeMon[i].innerHTML = monAbb[parseInt(time) - 1];
    }
    var articleTypes = ["新闻速递", "学术活动", "通知公告", "信息简报"];
    var nodeArticleType = document.getElementsByClassName("article-type-convert");
    for (var i = 0; i < nodeArticleType.length; i++) {
        var type = nodeArticleType[i].getAttribute("id");
        nodeArticleType[i].innerHTML = articleTypes[type - 1];
    }
    var libraryTypes = ["信息简报", "智库专刊", "蓝皮书"];
    var nodeLibraryType = document.getElementsByClassName("library-type-convert");
    for (var i = 0; i < nodeLibraryType.length; i++) {
        var type = nodeLibraryType[i].getAttribute("id");
        if (type == 4) {
            nodeLibraryType[i].innerHTML = libraryTypes[0];
        } else {
            nodeLibraryType[i].innerHTML = libraryTypes[type];
        }
    }
}