<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp"%>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="../index.jsp">首页</a>
            >
            <a href="javascript:;">人才培养</a>
            >
            <a href="2.jsp">研究成果</a>

        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>科学研究</h2>
                    <ul class="topNav">
                        <li class="a" style="height:auto;">

                            <a class="as" href="1.jsp">科研团队</a>


                        </li>
                        <li class="a">
                            <a class="as" href="2.jsp">科研项目</a>
                        </li>
                        <li class="a">
                            <a class="as" href="3.jsp">研究成果</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>研究成果</h2>
                    <div class="textHer">
                        <p>待更新...</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp"%>