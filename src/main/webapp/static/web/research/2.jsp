<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp"%>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="../index.jsp">首页</a>
            >
            <a href="javascript:;">人才培养</a>
            >
            <a href="2.jsp">科研项目</a>

        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>科学研究</h2>
                    <ul class="topNav">
                        <li class="a" style="height:auto;">

                            <a class="as" href="1.jsp">科研团队</a>


                        </li>
                        <li class="a">
                            <a class="as" href="2.jsp">科研项目</a>
                        </li>
                        <li class="a">
                            <a class="as" href="3.jsp">研究成果</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>科研项目</h2>
                    <div class="textHer">
                        <p>
                            自组建以来，原珠江-西江经济带发展研究中心在申请和承接国家层面的科研项目（比如国家社科基金、国家自然基金等）的同时，立足地区经济发展，围绕地区经济发展需要，开展相关科学研究。中心研究团队先后获得各级各类纵向项目18项，项目经费共计340万元。</p>
                        <table class="table table-bordered" style="background: #ffffff">
                            <caption>纵向项目一览表</caption>
                            <thead>
                            <tr>
                                <th>序号</th>
                                <th>项目类别</th>
                                <th>项目名称</th>
                                <th>负责人</th>
                                <th>立项年度</th>
                                <th>项目经费（万元）</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>国家社科基金重大项目</td>
                                <td>全面推进依法治国与促进西南民族地区治理体系和治理能力现代化研究</td>
                                <td>周世中</td>
                                <td>2014</td>
                                <td>60</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>国家社科基金重大项目</td>
                                <td>漓江流域上游区气候和植被变化的生态水文响应机制研究</td>
                                <td>翟禄新</td>
                                <td>2012</td>
                                <td>50</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>国家社科基金重大项目</td>
                                <td>快速城市化背景下的广西典型城市景观格局演化与生态效应机制研究</td>
                                <td>梁保平</td>
                                <td>2013</td>
                                <td>46</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>国家社科基金项目</td>
                                <td>儒学在古代岭南壮族地区的国家认同和社会治理中的作用研究</td>
                                <td>韦勇强</td>
                                <td>2014</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>国家社科基金项目</td>
                                <td>环境经济系统模型框架下的雾霾治理与区域经济结构升级研究</td>
                                <td>金明玉</td>
                                <td>2014</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>国家社科基金项目</td>
                                <td>边疆民族地区新型城镇化进程中民族文化交融机制研究</td>
                                <td>蒋士会</td>
                                <td>2014</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>国家社科基金项目</td>
                                <td>西南边疆民族地区丝绸之路经济带建设中城镇化多元发展研究</td>
                                <td>蒋团标</td>
                                <td>2015</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>国家社科基金项目</td>
                                <td>滇桂黔石漠化区农村土地流转调查和农民增收对策研究</td>
                                <td>罗顺元</td>
                                <td>2015</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>国家社科基金项目</td>
                                <td>珠江-西江经济带绿色走廊建设现状及对策研究</td>
                                <td>罗顺元</td>
                                <td>2016</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>国家社科基金项目</td>
                                <td>实现“一带一路”战略部署的西南边疆民族地区市场共同体建设路径研究</td>
                                <td>罗婧</td>
                                <td>2016</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td>国家社科基金项目</td>
                                <td>多维贫困视域下珠江-西江经济带精准扶贫与脱贫的机制与政策研究</td>
                                <td>廉超</td>
                                <td>2016</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>中国博士后科学基金第57批面上资助项目</td>
                                <td>珠江-西江经济带水资源生态补偿模式创新及政策研究</td>
                                <td>袁伟彦</td>
                                <td>2015</td>
                                <td>8</td>
                            </tr>
                            <tr>
                                <td>13</td>
                                <td>广西哲学社会科学规划研究课题重点项目</td>
                                <td>珠江—西江经济带产业协同发展研究</td>
                                <td>梁爱云</td>
                                <td>2015</td>
                                <td>3.5</td>
                            </tr>
                            <tr>
                                <td>14</td>
                                <td>广西哲学社会科学规划研究课题</td>
                                <td>珠江—西江经济带多维贫困测度与减贫策略研究</td>
                                <td>廉超</td>
                                <td>2015</td>
                                <td>2.5</td>
                            </tr>
                            <tr>
                                <td>15</td>
                                <td>广西哲学社会科学规划研究课题</td>
                                <td>珠江—西江流域生态补偿法律问题研究</td>
                                <td>潘庆</td>
                                <td>2015</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>16</td>
                                <td>广西教育科学“十二五”规划广西教育科学重点研究基地重大课题</td>
                                <td>珠江—西江经济带高校少数民族应用型人才培养政策与体制研究</td>
                                <td>卢蓬军</td>
                                <td>2015</td>
                                <td>5</td>
                            </tr>
                            <tr>
                                <td>17</td>
                                <td>广西高等学校科研项目重点项目</td>
                                <td>珠江流域中上游生态补偿理论研究——基于广西段的实证调查</td>
                                <td>罗顺元</td>
                                <td>2015</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>18</td>
                                <td>广西高校中青年教师基础能力提升项目</td>
                                <td>珠江—西江经济带的留守儿童教育问题与策略研究</td>
                                <td>杨茂庆</td>
                                <td>2016</td>
                                <td>2</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp"%>