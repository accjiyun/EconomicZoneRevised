<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp"%>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="../index.jsp">首页</a>
            >
            <a href="javascript:;">机构设置</a>
            >
            <a href="1.jsp">学术委员会</a>

        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>机构设置</h2>
                    <ul class="topNav">
                        <li class="a" style="height:auto;">

                            <a class="as" href="1.jsp">学术委员会</a>


                        </li>
                        <li class="a">
                            <a class="as" href="2.jsp">组织架构</a>
                        </li>
                        <li class="a">
                            <a class="as" href="3.jsp">研究中心</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>学术委员会</h2>
                    <div class="textHer">
                        <h3>珠江西江经济带发展研究中心学术委员会</h3>
                        <h4>主任委员：</h4>
                        <p>樊 杰，中国科学院地理科学与资源所二级研究员，中国科学院科技战略咨询研究院副院长，中国科学院可持续发展研究中心主任，国家规划专家委员会委员。</p><br>
                        <h4>副主任委员：</h4>
                        <p>林春逸，广西师范大学社科处处长，广西人文社会科学研究中心主任，教授。</p>
                        <p>
                            徐 毅，广西师范大学广西人文社会科学研究中心副主任，教授。
                        </p><br>
                        <h4>秘 书 长：</h4>
                        <p>李 晖，广西师范大学环境与资源学院，副教授。</p><br>
                        <h4>委 员（按姓氏拼音为序）：</h4>
                        <p>陈利顶，中国科学院生态环境研究中心二级研究员，城市和区域生态国家重点实验室主任，中国生态学学会秘书长。</p>
                        <p>刘祥学，广西师范大学历史文化与旅游学院院长，教授。</p>
                        <p>陆奇岸，广西师范大学经济管理学院院长，教授。</p>
                        <p>刘俊杰，广西师范大学经济管理学院，教授。</p>
                        <p>刘澈元，广西师范大学经济管理学院，教授。</p>
                        <p>李 晖，广西师范大学环境与资源学院，副教授。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp"%>