<%--
Created by IntelliJ IDEA.
User: jiyun
Date: 2016/11/24
Time: 10:37
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@include file="/top.jsp"%>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="../index.jsp">首页</a>
            >
            <a href="javascript:;">咨询服务</a>
            >
            <a href="1.jsp">横向课题</a>
        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>咨询服务</h2>
                    <ul class="topNav">
                        <li class="a">
                            <a href="">
                                <a class="as" href="1.jsp">横向课题</a>
                            </a>
                        </li>
                        <li class="a">
                            <a class="as" href="2.jsp">调研工作</a>
                        </li>
                        <li class="a">
                            <a class="as" href="3.jsp">最近项目</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>横向课题</h2>
                    <div class="textHer">

                        <p>
                            珠江-西江经济带发展研究中心依托现有优势特色学科的整体科研力量和团队优势，通过与区内外各级政府和企事业单位建立战略合作关系，积极主动承担实际部门的委托研究课题并开展合作研究，多层次地为政府、企事业单位提供战略发展规划及决策咨询、产品和技术开发与推广等方面的服务。现承担的主要横向课题包括：</p>
                        <ul>
                            <li>（1）珠江-西江经济带（武宣段）建设发展规划（2013-2020）</li>
                            <li>（2）广西高校创新驱动与科技发展十三五规划</li>
                            <li>（3）桂林市科技发展“十三五”编制</li>
                            <li>（4）永福县环境保护和生态建设“十三五”规划编制</li>
                            <li>（5）柳城县十二个乡镇的环境规划编制</li>
                            <li>（6）永福县乡镇饮用水源保护区划分技术报告</li>
                            <li>（7）象州县环境保护和生态建设“十三五”规划编制</li>
                            <li>（8）象州县“十三五”生态环境容量趋势研究</li>
                            <li>（9）永福县“十三五”生态环境容量趋势研究</li>
                            <li>（10）永福县环境保护和生态建设“十三五”规划编制</li>
                            <li>（11）柳城县“十三五”生态环境容量趋势研究</li>
                            <li>（12）柳城县环境保护和生态建设“十三五”规划编制</li>
                            <li>（13）兴安县环境保护和生态建设“十三五”规划编制</li>
                            <li>（14）兴安湘-漓流域突发水污染应急预案编制</li>
                            <li>（15）永福洛清江流域突发水污染应急预案编制</li>
                            <li>（16）永福苏桥自来水厂的环境影响评价报告表</li>
                            <li>（17）广西桂林会仙喀斯特国家湿地公园湿地保护与恢复工程建设项目环境影响评价报告书</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp"%>