<%--
Created by IntelliJ IDEA.
User: jiyun
Date: 2016/11/24
Time: 10:37
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@include file="/top.jsp"%>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="../index.jsp">首页</a>
            >
            <a href="javascript:;">咨询服务</a>
            >
            <a href="3.jsp">最近项目</a>
        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>咨询服务</h2>
                    <ul class="topNav">
                        <li class="a">
                            <a href="">
                                <a class="as" href="1.jsp">横向课题</a>
                            </a>
                        </li>
                        <li class="a">
                            <a class="as" href="2.jsp">调研工作</a>
                        </li>
                        <li class="a">
                            <a class="as" href="3.jsp">最近项目</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>最近项目</h2>
                    <div class="textHer">

                        <p>
                            珠江-西江经济带开放合作研究团队是珠江-西江经济带建设研究中心2015年启动建设的特色研究团队之一。受市财政绩效管理局的邀请，珠江-西江经济带开放合作团队汇聚了以广西师范大学经济管理学院为核心的财政、财务、税收、金融、审计及社会测量领域的精英，参与投标并顺利中标；于2016年3月正式承接公共自行车服务系统、无物业小区提升改造等五个项目，总计资金近2.8亿的第三方绩效评价工作。</p>
                        <p>
                            团队全程参与了桂林市本级财政绩效第三方评价活动，克服了工作量大繁琐、第三方身份不被重视、经费紧张等重重困难，不仅自身全力以赴，并充分结合自身的教学内容，带领本科生、研究生积极参与。工作过程中，共发放有效问卷逾1200多份；进行现场核验10余次，整理相关工作底稿近千万字；最终形成满意度问卷调查报告2份近10万字、绩效评价报告5份近20余万字。2016年5月10日，部分项目已顺利通过了面对面汇报，并赢得了市财政绩效管理局相关负责人的一致好评，赵卫局长盛赞团队老师们完成的绩效报告，“写得真好，这件事情委托你们来做真是太对了！” </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp"%>