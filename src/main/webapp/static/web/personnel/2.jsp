<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp"%>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="../index.jsp">首页</a>
            >
            <a href="javascript:;">人才培养</a>
            >
            <a href="2.jsp">培养成果</a>

        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>人才培养</h2>
                    <ul class="topNav">
                        <li class="a" style="height:auto;">

                            <a class="as" href="1.jsp">培养方案</a>


                        </li>
                        <li class="a">
                            <a class="as" href="2.jsp">培养成果</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>培养成果</h2>
                    <div class="textHer">
                        <p>硕、博士研究生参与科学研究是广西高校人文社科重点研究基地科教协同育人的重要任务。为了实现科教融合协同育人，为珠江-西江经济带发展培养高层次人才，中心专门设立了面向研究生培养的
                            “科学研究工程•珠江-西江经济带发展研究”科研项目。中心共拨出32万元用于启动此人才培育工程，每个项目经费1万元至1.5万元。</p>
                        <table class="table table-bordered" style="background: #ffffff">
                            <caption>“珠江-西江经济带发展研究”研究生专项项目一览表</caption>
                            <thead>
                            <tr>
                                <th>序号</th>
                                <th>课题名称</th>
                                <th>负责人</th>
                                <th>单位</th>
                                <th>学生类型</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>珠江-西江经济带社会矛盾治理中的社会组织参与研究</td>
                                <td>杨智勇</td>
                                <td>马克思主义学院</td>
                                <td>博士生</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>多维贫困视域下珠江——西江经济带生态扶贫策略研究</td>
                                <td>李玉雄</td>
                                <td>马克思主义学院</td>
                                <td>博士生</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>空间正义：珠江-西江经济带发展价值规约</td>
                                <td>李文兵</td>
                                <td>马克思主义学院</td>
                                <td>博士生</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>珠江—西江经济带民族地区社会治理法治化研究</td>
                                <td>凃耀军</td>
                                <td>马克思主义学院</td>
                                <td>博士生</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>水文化视域下珠江-西江经济带旅游资源整合开发及其品牌构建研究</td>
                                <td>赵娜</td>
                                <td>历史文化与旅游学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>珠江—西江经济带乡村旅游绿色扶贫模式研究</td>
                                <td>刘静静</td>
                                <td>历史文化与旅游学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>民国时期珠江-西江经济带环境变迁与社会应对研究</td>
                                <td>程美慧</td>
                                <td>历史文化与旅游学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>互动、调适与重构：近代西江流域的族群关系研究</td>
                                <td>王丽基</td>
                                <td>历史文化与旅游学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>珠江-西江经济带城乡分配正义研究</td>
                                <td>黄静秋</td>
                                <td>马克思主义学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>珠江—西江经济带生态补偿法律制度研究</td>
                                <td>马文斌</td>
                                <td>法学院/政治与行政管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td>珠江-西江经济带生态环境污染第三方治理研究</td>
                                <td>罗敏</td>
                                <td>法学院/政治与行政管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>珠江-西江经济带社会矛盾治理中的社会组织参与研究</td>
                                <td>黄婷</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>13</td>
                                <td>珠江—西江经济带江海联动模式研究</td>
                                <td>常玲</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>14</td>
                                <td>珠江-西江经济带建设“一带一路”门户与枢纽的市场共同体路径研究</td>
                                <td>贾明恺</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>15</td>
                                <td>珠江-西江经济带农业循环经济发展模式与评价研究</td>
                                <td>邓彪</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>16</td>
                                <td>珠江—西江经济带产业结构与分工的测度研究</td>
                                <td>郝健</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>17</td>
                                <td>FDI驱动珠江—西江经济带产业一体化发展的效应测评及促进策略</td>
                                <td>李宁</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>18</td>
                                <td>珠江—西江经济带高新产业园产城融合实证研究</td>
                                <td>宛睿</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>19</td>
                                <td>珠江—西江经济带产城融合发展模式与实施路径研究</td>
                                <td>宛睿</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>20</td>
                                <td>珠江-西江经济带高新技术产业技术创新效率实证研究</td>
                                <td>周浩</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>21</td>
                                <td>西江经济带重点开发区产业结构变动与能源利用效率关系研究</td>
                                <td>王艳琼</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>22</td>
                                <td>主体功能区视角下珠江-西江经济带城乡融合路径及对策研究</td>
                                <td>叶允最</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>23</td>
                                <td>珠江-西江经济带主要城市投资环境、经济发展潜力及其协调度数据库建设研究</td>
                                <td>曹志伟</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>24</td>
                                <td>珠江-西江经济带对接台湾建设两岸次区域市场共同体的路径与策略研究</td>
                                <td>刘方舟</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>25</td>
                                <td>珠江—西江经济带与东盟国家营商环境评价与区域功能定位问题研究</td>
                                <td>殷若楠</td>
                                <td>经济管理学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>26</td>
                                <td>产业转移视域下珠江—西江经济带职业教育布局对接泛珠三角产业结构的实证分析</td>
                                <td>夏光祥</td>
                                <td>教育学部</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>27</td>
                                <td>珠江—西江经济带民族文化融合与学校教育变迁研究</td>
                                <td>熊婷</td>
                                <td>教育学部</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>28</td>
                                <td>珠江-西江经济带开放合作背景下物流外宣英语翻译研究</td>
                                <td>李亚峰</td>
                                <td>外国语学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>29</td>
                                <td>大数据融合在西江-珠江经济带城际交通一体化中的应用与研究</td>
                                <td>何威</td>
                                <td>计算机科学与信息工程学院</td>
                                <td>硕士生</td>
                            </tr>
                            <tr>
                                <td>30</td>
                                <td>珠江—西江经济带生态脆弱区域生态环境预测与治理研究</td>
                                <td>万雷</td>
                                <td>电子工程学院</td>
                                <td>硕士生</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp"%>