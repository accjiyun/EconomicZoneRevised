<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp"%>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="../index.jsp">首页</a>
            >
            <a href="javascript:;">院情介绍</a>
            >
            <a href="3.jsp">研究方向</a>
        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">
                    <h2>院情介绍</h2>
                    <ul class="topNav">
                        <li class="a" style="height:auto;">
                            <a class="as" href="1.jsp">院情概况</a>
                        </li>
                        <li class="a">
                            <a class="as" href="2.jsp">发展目标</a>
                        </li>

                        <li class="a">
                            <a class="as" href="3.jsp">研究方向</a>
                        </li>
                        <li class="a">
                            <a class="as" href="4.jsp">联系方式</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>研究方向</h2>
                    <div class="textHer">
                        <h3>珠江-西江经济带发展研究院主要研究方向</h3>

                        <ul>
                            <li><strong>1.西南中南开放发展战略支撑带研究</strong></li>
                            <li><strong>2.东西部合作发展发展示范区研究</strong></li>
                            <li><strong>3.流域生态文化建设试验区研究</strong></li>
                            <li><strong>4.海上丝绸之路桥头堡研究</strong></li>
                        </ul>
                        <h4>发展研究院研究方向的形成基于整合广西师范大学自身学科力量，尤其是：</h4>
                        <p>
                            1.马克思主义理论学科：广西师范大学是广西高校中首批获得文科博士点、博士后流动站的单位，用全国首批马克思主义理论博士后科研流动站，马克思主义理论一级学科博士授权点，拥有自治区级协同创新中心“马克思主义理论与区域实践协同创新中心”；</p>
                        <p>2.生态学、环境科学：广西师范大学的“珍稀濒危动植物生态与环境保护”为省部共建教育部重点实验室。广西环境污染控制理论与技术重点实验室是自治区重点实验室；</p>
                        <p>3.中国语言文学：广西师范大学的中国语言文学是博士后科研流动站、博士授权点，“桂学研究”为首批广西“2011协同创新中心”；</p>
                        <p>4.应用经济学：广西师范大学的应用经济学是省级重点学科，是博士点培育建设学科；</p>
                        <p>5.专门史：广西师范大学的专门史是省级重点学科，是博士点培育建设学科；</p>
                        <p>
                            6.法学：广西师范大学的法学是省级重点学科，是博士点培育建设学科；信息与计算机科学：广西师范大学的广西多源信息挖掘与安全是省级重点实验室，实验室现拥有国家“千人计划”创新平台1个、广西“八桂学者”创新平台2个和广西自然科学基金创新研究团队1个</p>
                        <h4 style="text-indent: 2em;line-height: 200%;">
                            按照规划，发展研究院将在第二个建设周期（2015-2017年）内继续整合校外各高校相关学科力量，获得坚实的多学科支撑。</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp"%>