<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/top.jsp"%>
<div id="article-content">
    <div class="container">
        <p class="wzTitle"><strong>当前位置</strong>
            >
            <a href="../index.jsp">首页</a>
            >
            <a href="javascript:;">院情介绍</a>
            >
            <a href="3.jsp">联系方式</a>
        </p>
        <div class="subBanner">
            <span class="styleCom leftTop"></span>
            <span class="styleCom rightBottom"></span>

        </div>
        <div class="subCont">
            <div class="col-md-3">
                <div class="subLeft left">

                    <h2>院情介绍</h2>
                    <ul class="topNav">
                        <li class="a" style="height:auto;">
                            <a class="as" href="1.jsp">院情概况</a>
                        </li>
                        <li class="a">
                            <a class="as" href="2.jsp">发展目标</a>
                        </li>

                        <li class="a">
                            <a class="as" href="3.jsp">研究方向</a>
                        </li>
                        <li class="a">
                            <a class="as" href="4.jsp">联系方式</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right rightTExt">
                    <h2>联系方式</h2>
                    <div class="textHer">
                        <p style="margin-top: 20px;">地 址： 广西桂林市育才路15号 广西师范大学图书馆3楼</p>
                        <p>邮 编： 541004</p>
                        <p>电 话： 0773-5843330</p>
                        <p>传 真： 0773-5843330</p>
                        <p>邮 箱： skc@gxnu.edu.cn</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/footer.jsp"%>