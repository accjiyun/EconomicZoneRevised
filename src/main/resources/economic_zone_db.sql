/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : economic_zone_db

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2017-04-27 16:56:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `ARTICLE_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `ARTICLE_TYPE` int(4) NOT NULL DEFAULT '0' COMMENT '文章类型1:新闻速递,2:学术活动,3:通知公告,4：信息简报',
  `TITLE` varchar(100) DEFAULT NULL COMMENT '文章标题',
  `KEY_WORD` varchar(50) DEFAULT '' COMMENT '关键字',
  `CREATE_TIME` timestamp NULL DEFAULT NULL COMMENT '文章创建时间',
  `PUBLISH_TIME` timestamp NULL DEFAULT NULL COMMENT '文章发布时间',
  `STATUS` int(4) DEFAULT '0' COMMENT '发表状态：0为未发布，1为发布',
  `IMAGE_URL` varchar(255) DEFAULT NULL COMMENT '文章缩略图URL',
  `CLICK_NUM` int(11) DEFAULT '0' COMMENT '文章点击量',
  `SORT` int(11) DEFAULT '0' COMMENT '排序值',
  PRIMARY KEY (`ARTICLE_ID`),
  KEY `ARTICLE_TYPE` (`ARTICLE_TYPE`),
  CONSTRAINT `article_ibfk_1` FOREIGN KEY (`ARTICLE_TYPE`) REFERENCES `article_type` (`TYPE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '1', '习近平总书记于两会期间参加广西代表团审议', '两会,战略机遇', '2015-03-24 12:52:15', '2015-03-24 12:52:15', '1', '/images/upload/ueditor/image/list-bg1.jpg', '10', '0');
INSERT INTO `article` VALUES ('2', '1', '国务院批复同意《珠江-西江经济带发展规划》', '国务院', '2014-08-02 22:06:30', '2014-08-02 22:06:30', '1', '/images/upload/ueditor/image/list-bg2.jpg', '4', '0');
INSERT INTO `article` VALUES ('3', '1', '珠江-西江经济带建设研究中心被确定为广西高校人文社会科学重点研究基地', '人文社会科学', '2014-07-02 22:19:43', '2014-07-02 22:19:43', '1', '/images/upload/ueditor/image/list-bg4.jpg', '10', '0');
INSERT INTO `article` VALUES ('4', '1', '李克强总理在广西调研', '李克强,总理,调研', '2013-07-19 22:21:46', '2013-07-19 22:21:46', '1', '/images/upload/ueditor/image/list-bg3.jpg', '5', '0');
INSERT INTO `article` VALUES ('5', '2', ' 举办“珠江-西江经济带发展研究”高端论坛', '高端论坛', '2015-03-24 12:52:15', '2015-03-24 12:52:15', '1', '/images/upload/ueditor/image/xs1.jpg', '6', '0');
INSERT INTO `article` VALUES ('6', '2', '联合承办“新海上丝绸之路构建：从泛北部湾到欧洲”国际学术研讨会', '丝绸之路', '2014-09-02 22:06:30', '2014-09-02 22:06:30', '1', '/images/upload/ueditor/image/xs2.jpg', '9', '0');
INSERT INTO `article` VALUES ('7', '2', '联合承办第三届“中美论坛”', '中美论坛', '2015-05-02 22:37:39', '2015-05-02 22:37:39', '1', '/images/upload/ueditor/image/xs3.jpg', '2', '0');
INSERT INTO `article` VALUES ('8', '2', ' 联合主办“一带一路与两岸”学术会议在桂林召开', '一带一路,两岸', '2015-12-18 22:38:27', '2015-12-18 22:38:27', '1', '/images/upload/ueditor/image/xs4.jpg', '10', '0');
INSERT INTO `article` VALUES ('9', '3', '广西文科中心第77期系列学术沙龙', '广西文科中心', '2015-10-24 22:38:27', '2015-10-24 22:38:27', '1', '/favicon.ico', '7', '0');
INSERT INTO `article` VALUES ('10', '3', '第七十五期“人文强桂”学术沙龙', '人文强桂', '2015-09-10 12:52:15', '2015-09-10 12:52:15', '1', '/favicon.ico', '5', '0');
INSERT INTO `article` VALUES ('11', '3', '“珠江-西江智慧经济带建设”学术沙龙', '珠江-西江智慧经济带建设', '2015-08-20 12:52:15', '2015-08-20 12:52:15', '1', '/favicon.ico', '10', '0');
INSERT INTO `article` VALUES ('12', '3', '九十七期：“珠江-西江经济带城镇体系和城乡一体化研究”', '城镇体系,城乡,一体化', '2015-08-13 13:52:15', '2015-08-13 13:52:15', '1', '/favicon.ico', '14', '0');
INSERT INTO `article` VALUES ('13', '4', '桂林市副市长樊新鸿一行到我校调研', '副市长,樊新鸿', '2017-02-17 11:38:01', '2017-02-11 00:00:00', '1', '/favicon.ico', '11', '0');
INSERT INTO `article` VALUES ('16', '2', '测试文章标题', '测试关键字1,测试关键字2,测试关键字3', '2017-04-22 14:58:47', '2017-04-06 00:00:00', '0', '/images/upload/article/20170424/1493003071352.jpg', '9', '0');
INSERT INTO `article` VALUES ('17', '1', '中国青年网：广西师大研支团推出石桥计划培养学生综合素质', '研支团,石桥计划', '2017-04-23 19:56:35', '2017-04-23 00:00:00', '1', '/images/upload/article/20170427/1493283128293.png', '10', '0');

-- ----------------------------
-- Table structure for article_content
-- ----------------------------
DROP TABLE IF EXISTS `article_content`;
CREATE TABLE `article_content` (
  `ARTICLE_ID` int(11) NOT NULL DEFAULT '0' COMMENT '文章ID',
  `CONTENT` text COMMENT '文章内容',
  PRIMARY KEY (`ARTICLE_ID`),
  CONSTRAINT `article_content_ibfk_1` FOREIGN KEY (`ARTICLE_ID`) REFERENCES `article` (`ARTICLE_ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article_content
-- ----------------------------
INSERT INTO `article_content` VALUES ('1', '<p><img src=\"/images/upload/ueditor/image/new-2015-03.png\" alt=\"\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; float: left; margin-right: 30px; width: 260px; height: 200px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\"/></p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">2015年习近平总书记在全国两会期间参加广西代表团审议时对广西发展提出了新定位新要求：发挥广西与东盟国家陆海相连的独特优势，加快北部湾经济区和珠江-西江经济带开放开发，构建面向东盟的国际大通道，打造西南中南地区开放发展新的战略支点，形成21世纪海上丝绸之路和丝绸之路经济带有机衔接的重要门户。习近平总书记对广西发展的新定位新要求可以归纳为：“一个独特优势”、“两个战略机遇”、“三重角色定位”。其中，“两个重要战略机遇”就是加快北部湾经济区和珠江-西江经济带开放开发，加强跨区域合作，通过区域之间的产业布局优化、经济政策协同、统一市场建设等，形成区域发展互补支撑、优势叠加的格局，打造引领国际经济合作和竞争的开放区域。泛珠合作、粤桂合作、中国-东盟，走的正是这条路子，为此，亟待加强珠江-西江经济带发展研究，以更好地为国家发展战略服务。</p><p><br/></p>');
INSERT INTO `article_content` VALUES ('2', '<p><img src=\"/images/upload/ueditor/image/new-2014-08.jpg\" alt=\"\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; float: left; margin-right: 30px; width: 260px; height: 200px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\"/></p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">2014年8月国务院批复同意《珠江—西江经济带发展规划》。这是我国第一个横跨东西部、以流域经济合作为主题的区域规划。至此，珠江—西江经济带建设上升为国家发展战略。 经济带西接资源丰富的大西南，东联经济最活跃的珠三角，横贯两广并直达港澳，与国际海运网对接，不仅是两广水上交通大动脉，也是构筑泛北部湾经济合作区与建设中国—东盟自由贸易区的一个重要出海口 ，是连接“２１世纪海上丝绸之路”与“丝绸之路经济带”的桥头堡，有机衔接的重要门户。</p><p><br/></p>');
INSERT INTO `article_content` VALUES ('3', '<p><img src=\"/images/upload/ueditor/image/new-2014-07.jpg\" alt=\"\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; float: left; margin-right: 30px; width: 260px; height: 200px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\"/></p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">2014年7月，根据《关于订定广西高校人文社会科学重点研究基地的通知》（桂教社科〔2014〔12号〕，珠江-西江经济带建设研究中心确定为2014年度广西高校人文社会科学重点研究基地。目前，中心地点设立在广西师范大学图书馆三楼，办公面积约80平方米。中心办公设施基本配备齐全，设有主任、副主任办公室，秘书办公室、学术交流中心等。中心突出问题导向，服务国家与区域发展战略，通过多学科、多部门、跨区域、跨境跨国集成研究、协同创新，围绕珠江-西江经济带合作发展亟待解决的重大现实问题开展理论研究和应用对策研究。</p><p><br/></p>');
INSERT INTO `article_content` VALUES ('4', '<p><img src=\"/images/upload/ueditor/image/new-2013-07.jpg\" alt=\"\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; float: left; margin-right: 30px; width: 260px; height: 200px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\"/></p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">2013年7月，李克强总理在广西调研时强调指出，“广西属于西部地区，是我国中西部唯一具有沿海港口的省份，又是民族地区、革命老区、边境地区、沿江地区，广西发展中遇到的困难、问题具有很强的代表性。”并指出，“就全国来说，最大的不平衡是城乡之间、区域之间发展不平衡，最大的升级是城乡之间、区域之间的结构调整和协调发展。只有东中西部、城乡协调发展，中国经济才能真正转型升级。”还强调，“对广西和中西部地区的发展，中央要加大支持力度，东部地区也要大力支持，这是关系全国发展的大格局”。 通过促进珠江-西江经济带合作、开放开发，解决珠江流域区域之间协调发展难题，包括广西遭遇到的发展难题。</p><p><br/></p>');
INSERT INTO `article_content` VALUES ('5', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    2015年3月27-29日，“珠江-西江经济带发展研究”高端论坛在我举行。我校副校长钟瑞添、自治区教育厅科研处傅源方处长出席开幕式并分别致辞。来自北京大学、中国社会科学院、中国科学院、广西自治区发改委、中山大学、华东理工大学、河北师范大学、河南大学、贵州师范大学、广州大学、广东社会科学院、广西社会科学院、广西师范大学、广西大学、广西师范学院、广西科技大学、梧州学院等高校、科研院所、政府部门近50名专家学者参加了本次会议。\r\n</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    在为期一天半的集中高效的研讨中，来自马克思主义理论、经济学、生态学、地理学、环境科学、管理学、民族学、社会学、伦理学等多学科的专家学者聚焦珠江-西江经济带的协同发展、创新发展、开放发展、协调发展、合作发展等关键问题，发表了各自研究领域的真知灼见，在交流碰撞中对重点议题进行了广泛深入的探讨和前瞻性的思考，对珠江-西江经济带发展提供了有力支撑，对进一步明晰我校珠江-西江经济带发展研究中心建设思路提供了重要参考。 论坛受到了各级媒体的高度关注和转载报道。\r\n</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    相关报道链接：\r\n</p>\r\n<ul class=\" list-paddingleft-2\">\r\n    <li>\r\n        <p>\r\n            <a href=\"http://www.kaixian.tv/gd/2015/0328/74008.html\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">http://www.kaixian.tv/gd/2015/0328/74008.html</a>中新网\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://news.ifeng.com/a/20150328/43437057_0.shtml\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">http://news.ifeng.com/a/20150328/43437057_0.shtml</a>凤凰网资讯（转载）\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://cnews.chinadaily.com.cn/2015-03/30/content_19947908.htm\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">http://cnews.chinadaily.com.cn/2015-03/30/content_19947908.htm</a>中国日报网（转载）\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://roll.sohu.com/20150328/n410471021.shtml\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">http://roll.sohu.com/20150328/n410471021.shtml</a>搜狐网（转载）\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://ethn.cssn.cn/zx/zx_gjzh/zhnew/201503/t20150328_1564949.shtml\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">http://ethn.cssn.cn/zx/zx_gjzh/zhnew/201503/t20150328_1564949.shtml</a>中国社会科学网 （转载）\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://roll.sohu.com/20150328/n410471021.shtml\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">http://roll.sohu.com/20150328/n410471021.shtml</a>中国台湾网（转载）\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://society.people.com.cn/n/2015/0328/c136657-26764905.html\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">http://society.people.com.cn/n/2015/0328/c136657-26764905.html</a>人民网（转载）\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://news.163.com/15/0328/18/ALQIV7DA00014JB6.html\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">http://news.163.com/15/0328/18/ALQIV7DA00014JB6.html</a>网易网（转载）\r\n        </p>\r\n    </li>\r\n    <p style=\"text-align: center;\">\r\n        <img src=\"/images/upload/ueditor/image/activity1-1.jpg\" alt=\"\" width=\"500\" height=\"400\"/>\r\n    </p>\r\n    <h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n        河南大学黄河文明与可持续发展研究中心主任 苗长虹教授\r\n    </h3>\r\n    <p style=\"text-align: center;\">\r\n        <img src=\"/images/upload/ueditor/image/activity1-2.jpg\" alt=\"\" width=\"500\" height=\"400\"/>\r\n    </p>\r\n    <h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n        北京大学光华管理学院经济研究所副所长颜色\r\n    </h3>\r\n    <p style=\"text-align: center;\">\r\n        <img src=\"/images/upload/ueditor/image/activity1-3.jpg\" alt=\"\" width=\"500\" height=\"400\"/>\r\n    </p>\r\n    <h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n        广西自治区发改委黄汝焜总经济师\r\n    </h3>\r\n    <p style=\"text-align: center;\">\r\n        <img src=\"/images/upload/ueditor/image/activity1-4.jpg\" alt=\"\" width=\"500\" height=\"400\" title=\"\"/>\r\n    </p>\r\n    <h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n        广东社科院国际经济研究所刘毅研究员\r\n    </h3>\r\n</ul>\r\n<p>\r\n    <br/>\r\n</p>');
INSERT INTO `article_content` VALUES ('6', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    2014年9月25日，由广西人文社会科学发展研究中心、泛北部湾区域研究协同创新中心主办，珠江-西江经济带发展研究中心、经济管理学院联合承办的 “新海上丝绸之路构建：从泛北部湾到欧洲”国际学术研讨会在桂林市桂湖饭店举行。中国科学院经济学研究所副所长刘兰兮、广西教育厅研究处处长傅源方、我校副校长钟瑞添、荷兰乌特勒支大学全球经济史研究中心研究员、意大利、英国的相关研究专家学者以及来自云南社科院、福建社科院、台湾中华研究院、清华大学、复旦大学、中山大学、苏州大学、台湾云林科技大学、香港科技大学等各高校的相关专家和学者参加了研讨会。重点聚焦泛北部湾、珠江-西江经济带与“海上丝绸之路”战略对接问题。\r\n</p>\r\n<p style=\"text-align: center;\">\r\n    <img src=\"/images/upload/ueditor/image/activity2-1.jpg\" alt=\"\" width=\"500\" height=\"400\" title=\"\"/>\r\n</p>\r\n<h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    中国社科院经济研究所副所长刘兰兮致辞\r\n</h3>\r\n<p style=\"text-align: center;\">\r\n    <img src=\"/images/upload/ueditor/image/activity2-2.jpg\" alt=\"\" width=\"500\" height=\"400\" title=\"\"/>\r\n</p>\r\n<h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    台湾学者欧阳承新在会场发言\r\n</h3>\r\n<p style=\"text-align: center;\">\r\n    <img src=\"/images/upload/ueditor/image/activity2-3.jpg\" alt=\"\" width=\"500\" height=\"400\" title=\"\"/>\r\n</p>\r\n<h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    荷兰乌特勒支大学Bas van leeuwen研究员发言\r\n</h3>\r\n<p style=\"text-align: center;\">\r\n    <img src=\"/images/upload/ueditor/image/activity2-4.jpg\" alt=\"\" width=\"500\" height=\"400\" title=\"\"/>\r\n</p>\r\n<h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    英国华威大学Bishnupriya Gupta教授发言\r\n</h3>\r\n<p>\r\n    <br/>\r\n</p>');
INSERT INTO `article_content` VALUES ('7', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    2015年5月9日至10日，由中国社会科学杂志社、美国维思里安大学主办，广西师范大学、广西人文社会科学发展研究中心、珠江-西江经济带发展研究中心承办的第三届中美学术高层论坛在我校举行。本次论坛来自中国社会科学院、中国社会科学杂志社、美国维思里安大学、纽约大学、佛吉尼亚大学，英国伦敦政治经济学院，台北东吴大学、中央党校、复旦大学、武汉大学、南京大学、厦门大学、中山大学、华中师范大学、深圳大学、广西师范大学等科研院所、高等学校的30余专家学者围绕“现代化—中西比较的视野”这一主题，展开了7场学术探讨与思想交流。\r\n</p>\r\n<p style=\"text-align:center\">\r\n    <img src=\"/images/upload/ueditor/image/activity3-1.jpg\" alt=\"\"/>\r\n</p>\r\n<h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    第三届中美学术高层论坛开幕式现场\r\n</h3>\r\n<p style=\"text-align: center;\">\r\n    <img src=\"/images/upload/ueditor/image/activity3-2.jpg\" alt=\"\"/>\r\n</p>\r\n<h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    中国社会科学院秘书长中国社会科学杂志社总编辑高翔在论坛开幕式上发表致辞\r\n</h3>\r\n<p style=\"text-align:center\">\r\n    <img src=\"/images/upload/ueditor/image/activity3-3.jpg\" alt=\"\"/>\r\n</p>\r\n<h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    美国维思里安大学校长迈克尔•罗斯发表致辞\r\n</h3>\r\n<p style=\"text-align:center\">\r\n    <img src=\"/images/upload/ueditor/image/activity3-4.jpg\" alt=\"\"/>\r\n</p>\r\n<h3 style=\"box-sizing: border-box; font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(51, 51, 51); margin: 20px auto 30px; font-size: 24px; text-align: center; max-width: 500px; white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    中国社会科学杂志社学术新媒体平台\r\n</h3>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">\r\n    此次中美学术高层论坛受到学术界的广泛关注。中国社会科学网向全球同步直播，并首次实现PC端与移动端同时播出，取得了良好传播效果。到闭幕式召开之时，会议专题内容的微博点击量超过了23万余次。多家媒体给予报道：\r\n</p>\r\n<ul class=\" list-paddingleft-2\">\r\n    <li>\r\n        <p>\r\n            <a href=\"http://www.gx.chinanews.com/2015/1603_0510/125156.html\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">中新社：第三届中美学术高层论坛在广西桂林举行</a>\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://www.gx.xinhuanet.com/dtzx/guilin/2015-05/12/c_1115260317.htm\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">新华社：第三届中美学术高层论坛在广西桂林开幕</a>\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://www.cssn.cn/ddzg/2013xsdt/201505/t20150511_1764161.shtml\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">中国社会科学网：第三届中美学术高层论坛聚焦“现代化”</a>\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://stv.cssn.cn/index.php?option=com_content&id=1724\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">中国社会科学网社科电视：</a>\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://www.china.com.cn/opinion/think/2015-05/12/content_35548967.htm\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">中国网 智库中国：第三届中美学术高层论坛聚焦“现代化”</a>\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://difang.gmw.cn/newspaper/2015-05/15/content_106624784.htm\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">光明网：专家聚师大 共话“现代化”</a>\r\n        </p>\r\n    </li>\r\n    <li>\r\n        <p>\r\n            <a href=\"http://www.gx211.com/news/2015512/n6658260023.html\" style=\"box-sizing: border-box; background-color: transparent; color: rgb(66, 139, 202); text-decoration: none;\">中国高校之窗：第三届中美学术高层论坛在广西师范大学举行</a>\r\n        </p>\r\n    </li>\r\n</ul>\r\n<p>\r\n    <br/>\r\n</p>');
INSERT INTO `article_content` VALUES ('8', '<p><img src=\"/images/upload/ueditor/image/activity4-1.jpg\" alt=\"\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; float: left; margin-right: 30px; width: 260px; height: 200px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, Tahoma, Arial, Helvetica, sans-serif; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\"/></p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; line-height: 32px; font-family: 宋体; text-indent: 2em; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">2015年11月20日由联合主办的“一带一路与两岸”学术会议在桂林市召开。来自中国社会科学院、厦门大学、广西师范大学、台湾政治大学、南台湾两岸关系协会等两岸各高校与研究机构的40余位知名专家学者参加。本次会议以“两岸在一带一路战略实施中的合作”为主题，研讨主要围绕三个议题展开：“一带一路”建设对两岸关系的影响；台湾参与“一带一路”建设的路径与机制；两岸合作面向国际推动“一带一路”建设。</p><p><br/></p>');
INSERT INTO `article_content` VALUES ('9', '<p><span style=\"color: rgb(51, 51, 51); font-family: 宋体; text-indent: 32px; background-color: rgb(255, 255, 255);\">&nbsp; 广西文科中心第77期系列学术沙龙定于2015年10月24日下午在文科中心会议室举行。此次学术沙龙主题将聚焦国家级区域发展新战略——珠江-西江经济带区域发展。沙龙将由珠江-西江经济带发展研究中心主任林春逸教授、广西文科中心副主任陈小燕教授、珠江-西江经济带研究中心专家刘澈元教授、徐毅教授、李晖副教授等共同主持，各相关学院专家和在校博士、硕士研究生将参加本次沙龙研讨。</span></p>');
INSERT INTO `article_content` VALUES ('10', '<p><span style=\"color: rgb(51, 51, 51); font-family: 宋体; text-indent: 32px; background-color: rgb(255, 255, 255);\">&nbsp;&nbsp;广西文科中心“泛北部湾研究团队”将于2015年9月10日晚在文科中心学术活动室举办第七十五期“人文强桂”学术沙龙。沙龙将就新海上丝绸之路的发展问题进行新的尝试和探索。我校历史文化与旅游学院、经济管理学院及桂林师专等单位师生将参加本次学术沙龙。此次沙龙讨论将进一步明确新海上丝绸之路在经济建设中的重要作用，尤其是实现东西部协调发展。同时针对在国际秩序重建进程中新海丝的构建仍面临着的风险和挑战，沙龙将致力于探讨如何从现有合作机制着手，搭建战略平台，做足“海”的文章，推进丝绸之路经济带和海上丝绸之路的建设，形成全方位开放的新格局已成为现今研究的责任和使命。</span></p>');
INSERT INTO `article_content` VALUES ('11', '<p><span style=\"color: rgb(51, 51, 51); font-family: 宋体; text-indent: 32px; background-color: rgb(255, 255, 255);\">&nbsp; 广西文科中心“珠江-西江智慧经济带建设研究团队”沙龙将于8月20日在广西文科中心举行。沙龙将由两位团队首席专家周劲波教授、彭润华教授主持，研究团队全体成员以及相关教师和研究生将参加此次学术沙龙，同时欢迎其他各界人士参与。</span></p>');
INSERT INTO `article_content` VALUES ('12', '<p>&nbsp;&nbsp;<span style=\"color: rgb(51, 51, 51); font-family: 宋体; text-indent: 32px; background-color: rgb(255, 255, 255);\">广西文科中心人文强桂系列学术沙龙之九十七期“珠江-西江经济带城镇体系和城乡一体化研究”学术沙龙将于8月13日在育才校区图书馆西侧313室召开。活动将由广西师范大学经济管理学院阳芳教授主持，同时邀请蒋团标教授、刘俊杰教授分别作主题发言。此次沙龙将邀请该团队研究人员、桂林理工大学管理学院的部份专家以及我校经济管理学院部分研究生出席。</span></p>');
INSERT INTO `article_content` VALUES ('13', '<p>\n    <span style=\'color: rgb(51, 51, 51); font-family: \"Segoe UI\", Verdana, Helvetica, sans-serif; background-color: rgb(255, 255, 255);\'>　　近日，桂林市副市长樊新鸿一行到我校就“创新创业”、“服务地方经济”作主题调研。校长古天龙、副校长徐华蕊热情接待了樊副市长一行。</span><br><span style=\'color: rgb(51, 51, 51); font-family: \"Segoe UI\", Verdana, Helvetica, sans-serif; background-color: rgb(255, 255, 255);\'>　　古校长对樊副市长一行的到来表示热烈的欢迎，并重点推介了罗笑南教授和孙希延教授这两个代表团队。樊副市长先后参观了广西信息科学实验中心、广西壮族自治区位置感知与位置服务技术工程实验室，认真听取了罗教授和孙教授的工作汇报，对两个团队所取得的科研成果给予充分的肯定。随后在中心会议室举行了交流座谈。</span><br><span style=\'color: rgb(51, 51, 51); font-family: \"Segoe UI\", Verdana, Helvetica, sans-serif; background-color: rgb(255, 255, 255);\'>　　会上，就建立桂电科技园和研究院、提升服务地方经济的能力等方面进行了交流探讨。樊副市长对桂电的发展规划表示支持和赞赏，希望学校利用好资源，转化高新技术，吸引优质企业、聚集尖端人才，配合桂林市政府打赢“重振桂林工业雄风”的战役。双方还就产业园建设、研究院建设、人才引进、吸引优质企业落户、发动校友回桂林创业、“双一流”建设、科研项目申报等议题进行了深入探讨，达成合作意向。</span><br><span style=\'color: rgb(51, 51, 51); font-family: \"Segoe UI\", Verdana, Helvetica, sans-serif; background-color: rgb(255, 255, 255);\'>　　市工信委、科技局、高新区、招商局和我校科技处、计算机与信息安全学院相关领导参加了此次调研。</span><br><span style=\'color: rgb(51, 51, 51); font-family: \"Segoe UI\", Verdana, Helvetica, sans-serif; background-color: rgb(255, 255, 255);\'>　　（供稿：科技处&nbsp; 沈世铭&nbsp;&nbsp;&nbsp; 审稿：宣传部&nbsp; 朱艳萍）</span>\n</p>');
INSERT INTO `article_content` VALUES ('16', '<pre><span>public static void </span><span>main</span>(String[] args) {<br><br>    ArrayList&lt;Integer&gt; inputs = <span>new </span>ArrayList&lt;Integer&gt;()<span>;<br></span><span>    </span>Scanner in = <span>new </span>Scanner(System.<span>in</span>)<span>;<br></span><span>    </span>String line = in.nextLine()<span>;<br></span><span>    if</span>(line != <span>null </span>&amp;&amp; !line.isEmpty()) {<br>        <span>int </span>res = <span>resolve</span>(line.trim())<span>;<br></span><span>        </span>System.<span>out</span>.println(String.<span>valueOf</span>(res))<span>;<br></span><span>    </span>}<br>}</pre>');
INSERT INTO `article_content` VALUES ('17', '<p>　　近期，广西师范大学研支团的“粉笔头”分别以麦岭初中、葛坡希望小学、石桥教学点为单位开设“石桥”计划系列课程，课程内容首先涵盖中国传统文化、爱国主义教育、体育运动与健康、文明礼仪四个层面，把社会价值体系融入教育全过程，使学生开阔视野，丰富知识，提高乡村中、小学生的综合素质，培养合格公民。</p><p>中国传统文化系列课程目前已开设两个课时，研支团成员王思衍结合课文《花木兰》的学习，组织同学们用话剧的形式演绎了巾帼英雄花木兰的故事。在同学们了解故事历史背景、花木兰形象和我国话剧的特色后，把课堂的主动权交给学生，让其自编自导自演，成功的把“爱国主义”“巾帼英雄”“话剧”等要素结合起来，表演精彩纷呈。课后，同学们纷纷表达了对这种授课形式的喜爱和对传统文化的新认识。</p><p>　　王思衍还在葛坡希望小学组织同学们绘画，带同学们走进艺术课堂，领略了中国传统艺术的美妙，了解祖国的灿烂文化，悠久历史，让学生在灵魂深处夯筑起民族文化殿堂的基础。</p><p>　　中小学是树立正确的人生观、价值观、世界观的关键阶段，研支团成员王婷婷运用唱歌、绘画、观看影视资料等生动、有效的形式在麦岭初中和石桥教学点开设了两个课时的爱国主义教育课程。带领学生通过学唱国歌、描绘五星红旗、认识国徽、观看《国强则少年强》演讲视频、“祖国—我想对您说”等多个环节深入开展爱国主义教育活动，营造了浓郁的爱国主义教育氛围，激发了同学们热爱祖国、报效祖国的雄心壮志。</p><p>为了激发学生的运动兴趣,养成坚持体育锻炼的习惯，了解体育运动与健康的相关知识，研支团成员张明坤在葛坡希望小学通过素质拓展的形式为孩子们上了一堂生动的体育运动与健康课，首先通过“大西瓜、小西瓜”的活动锻炼了孩子们肢体与语言的协调性，然后利用“吹橡皮”小游戏锻炼了孩子们的肺活量，最后又为孩子们讲解平时体育锻炼中需要注意的事项，从日常生活中各个方面向孩子们渗透体育健康教育。</p><p>　　文明礼仪是中华民族的传统美德，针对当前农村中、小学生文明礼仪意识淡薄的状况，研支团成员覃震开设了文明礼仪系列课程。用“一双筷子”将孩子们带进了餐桌文明礼仪的课堂，从入座、进餐、使用餐具注意事项等多方面向孩子们讲解了餐桌礼仪，孩子们学习热情高涨，课后很多学生表示收获颇丰。在石桥教学点，覃老师带领孩子们通过即兴表演的形式，为孩子们上了一堂生动的文明出行礼仪课，让孩子们真正理解了文明礼仪的重要性，争做一个讲文明、懂礼仪的好孩子。</p><p>　　学生综合素质的培养，是中小学生身心发展的内在要求。广西师大研支团的“石桥”计划系列课程旨在提升学生综合素质，推动学生全面发展。据悉，科普教育、生理健康、法治教育等系列课程也将分批、分阶段陆续推出。</p><p><br></p><p>原文报道链接：<a href=\"http://xibu.youth.cn/gzdt/gddt/201704/t20170412_9462102.htm\" _src=\"http://xibu.youth.cn/gzdt/gddt/201704/t20170412_9462102.htm\">http://xibu.youth.cn/gzdt/gddt/201704/t20170412_9462102.htm</a><br></p><p><br></p><p><br></p><p style=\"text-align: center;\"><img title=\"W020170412328144531472.jpg\" alt=\"W020170412328144531472.jpg\" src=\"http://news.gxnu.edu.cn/upload/image/20170420/6362830200507814846445162.jpg\"></p><p style=\"text-align: center;\">广西师大研支团成员王思衍结合课文《花木兰》的学习，组织同学们用话剧的形式演绎巾帼英雄花木兰的故事。</p><p><br></p><p style=\"text-align: center;\"><img title=\"W020170412328144564104.jpg\" alt=\"W020170412328144564104.jpg\" src=\"http://news.gxnu.edu.cn/upload/image/20170420/6362830202705858701322643.jpg\"></p><p style=\"text-align: center;\">广西师大研支团成员张明坤在葛坡希望小学通过素质拓展的形式为孩子们上一堂生动的体育运动与健康课。</p>');

-- ----------------------------
-- Table structure for article_type
-- ----------------------------
DROP TABLE IF EXISTS `article_type`;
CREATE TABLE `article_type` (
  `TYPE_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '类型ID',
  `TYPE_NAME` varchar(30) DEFAULT '' COMMENT '文章类型名',
  `CREATE_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article_type
-- ----------------------------
INSERT INTO `article_type` VALUES ('1', '新闻速递', '2016-11-27 09:57:36');
INSERT INTO `article_type` VALUES ('2', '学术活动', '2016-11-27 09:57:36');
INSERT INTO `article_type` VALUES ('3', '通知公告', '2016-11-27 09:57:36');
INSERT INTO `article_type` VALUES ('4', '信息简报', '2016-11-27 09:57:36');

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES ('12');

-- ----------------------------
-- Table structure for library
-- ----------------------------
DROP TABLE IF EXISTS `library`;
CREATE TABLE `library` (
  `LIBRARY_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '智库ID',
  `LIBRARY_TYPE` int(4) DEFAULT '0' COMMENT '智库类型1:智库专刊,2:蓝皮书',
  `TITLE` varchar(100) DEFAULT NULL COMMENT '智库标题',
  `SUMMARY` text COMMENT '摘要',
  `IMAGE_URL` varchar(255) DEFAULT NULL COMMENT '智库图片URL',
  `PUBLISH_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '智库发布时间',
  `CREATE_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '智库创建时间',
  `LIBRARY_URL` varchar(255) DEFAULT NULL COMMENT '智库文件URL',
  `DOWNLOAD_NUM` int(11) unsigned zerofill DEFAULT '00000000000' COMMENT '下载数',
  PRIMARY KEY (`LIBRARY_ID`),
  KEY `LIBRARY_TYPE` (`LIBRARY_TYPE`),
  CONSTRAINT `library_ibfk_1` FOREIGN KEY (`LIBRARY_TYPE`) REFERENCES `library_type` (`TYPE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of library
-- ----------------------------
INSERT INTO `library` VALUES ('1', '1', '推进珠江－西江经济带产业承接区 多规合一协调发展的对策建议', '<p><span><b>内容摘要：</b></span>珠江-西江经济带（以下简称珠西经济带）两广毗邻的产业承接区，资源禀赋、发展水平相近，存在要素集聚与产业合作的区位基础。但受制于不同发展目标的多元规划与政策意向，难以形成协同效应。为推进珠西经济带一体化发展，必须从经济带发展的国家战略和比较优势出发，聚焦多元规划中的区域协同重点，优化协调国家战略与地方目标的关系，统筹区域发展整体框架。本文提出以粤桂合作特别试验区为切入点，以一体化目标引导要素的自由流动和产业布局，探索多规合一、合作共建、利益共享的区域发展新机制。</p><p><b><span>关键词：珠江-西江经济带 产业承接区 多规合一</span><span>&nbsp;</span></b><br><br></p><p>根据《珠江-西江经济带发展规划》，粤桂两省区毗邻地域的肇庆、云浮、梧州、贵港等城市，构成经济带四大组团中的“产业承接组团”。作为产业承接区，要发挥承东启西的区位优势，加强与珠三角及港澳地区的合作联动、配套发展，高起点承接产业转移，引导产业集聚，促进粤桂毗邻地区港口、产业、城镇融合发展。规划重在落实。战略规划既要考虑短、中期的策略，更要具有长期的战略视野。特别当两个发展阶段具有明显梯度差异的省区，在涉及国家战略和地区利益刚性，涉及相邻区域发展阶段、要素禀赋与分工合作的比较优势、涉及经济带总体战略和本行政区发展导向时，需要通过共同的长期目标以及适当的可操作的激励机制，通过规划协调、利益共享等制度设计，促进经济带协同发展。</p><p><span><b>一、主动对接珠三角，拓宽产业转移空间</b></span></p><p>（一）衔接“两个规划”、“一个意见”</p><p>衔接区域发展中的相关规划，实现多规合一和激励相容，有助于持续发展和政策红利叠加释放。珠西经济带“产业承接组团”面临与产业转移相关的三大规划。早在2008年国务院批准并发布《珠江三角洲地区改革发展规划纲要（2008－2020年）》（简称“珠三角规划”），该规划首次在国家规划中覆盖港澳地区，为珠三角特别是粤港澳的互动发展作出了前瞻性战略安排。经过多年实施，珠三角转型升级成效显著。2014年推出的“珠西规划”，在规划区域上与“珠三角规划”存在交集，将珠三角区域以北的广州、佛山以及向西延伸的肇庆、云浮等列入“珠西规划”版图，规划同样以2020年为限。上述规划相继出台后，2016年3月国务院又发布《关于深化泛珠三角区域合作的指导意见》。以“9+2”各方为背景，空间上涵盖了珠西经济带几乎全部区域。因此，在“珠西规划”的具体实践中，承接广东产业转移的同时，必然应充分考虑与粤港澳“泛珠合作区”、“珠三角规划”的衔接，以“一体化、同城化、特区化”机制导向，统一规划、多规合一，全方位推进并强化东向战略。唯如此，才能真正发挥多个国家层面的规划在该地区承接产业转移战略政策红利的叠加。</p><p>（二）拓展产业转移空间</p><p>审视“两个规划”、“一个意见”的战略重点，可把握泛珠三角、珠三角和珠西经济带融合互动的发展逻辑。“珠三角规划”的重点是：结构战略性调整、参与国际合作竞争、辐射带动环珠江三角洲区域经济发展、保持港澳地区长期繁荣稳定、深化体制机制创新。“珠西规划”的重点是：经济提质增效升级、培育新的区域经济带、缩小区域差距、完善全方位开放格局、跨省区流域生态共建。而“泛珠合作意见”提出建设承接产业转移示范区，支持沿海产业及国内外知名企业生产基地向中西部有序转移，促进产业组团式承接和集群式发展，推进粤桂黔高铁经济带合作试验区、粤桂合作特别试验等跨省区合作平台发展。加强内地与港澳紧密合作，推动扩大内地与港澳企业相互投资，鼓励和支持内地与港澳企业共同“走出去”。</p><p>可见，以珠三角为核心的先发地区的产业扩散，通过沿珠江流域有序推移是必然选择。利用“珠三角规划”、“珠西规划”和“泛珠合作意见”的地域交集，从机制层面进一步细化“两规划”、“一意见”具体操作措施的衔接，充分释放国家级规划和政策导向对本地区发展的引导效用。深化两广毗邻区特别是粤桂合作特别试验区与珠三角包括港澳在内的城市群，以及和东盟国家的产业合作，发挥市场机制，注重比较优势，提升产业合作绩效，实现互利共赢。</p><p><span><b>二、优化协调主体功能，推进产业与生态融合</b></span></p><p>（一）审视不同主体功能区的关系</p><p>国务院于2011年发布的《全国主体功能区规划》是我国国土空间开发的战略性、基础性和约束性规划。《规划》对于推进形成人口、经济和资源环境相协调的国土空间开发格局，加快转变经济发展方式具有重要战略意义。其后各省区也相继编制了次级主体功能区规划。</p><p>就珠西经济带产业承接区而言，位于西江流域的肇庆和云浮大部分区域被广东列入重点生态功能区或国家级粮食主产区，而与之相邻的岑溪、梧州城区以及贺州的八步区则被广西列为重点开发区域，从西江经济带广西段的主体功能定位看，广西的“双核驱动”战略将西江经济带确定为推进区域发展的新“增长极” 。上游作为重点开发区，下游作为重点生态功能区，显然有悖于流域产业布局和生态环境优化的基本逻辑。在现行的区划框架下，建议由国家相关部门进行协调，一是将上游重点开发区的各类园区尽可能集中布局，便于产业集聚和实现规模效应；二是各利益方共同承担对生态环保的投入，将生态补偿与产业优化转型结合起来，入园企业必须依法排放和优化转型。合理布局建设项目，拒绝高能耗、高排放企业落户特别试验区，建设生态环保产业集群，打造“流域生态文明建设试验区”。</p><p>（二）审视产业迁移与产业聚集的关系</p><p>改变发展中地区以级差地租和劳动力丰裕的比较优势吸引发达地区产业转移的传统观念。以制造业迁移集聚为例，报酬递增理论认为制造业的竞争优势，不仅在于要素的成本，还在于产业的空间集聚和这种集聚所带来的规模经济效应。由此说明，即便是在广东境内，2005年至今广东省政府推动在粤东西北地区建立的40个产业转移工业园，表现出“东西北方热、珠三角方冷”的现象，产业转移园建设对珠三角转出方的激励较弱。况且，企业迁移并不代表产业的转移，珠三角鼓励的“腾笼换鸟”只是希望将产业水平和生产率低下的企业转出，珠西经济带产业承接区应以更高的视野考虑承接产业转移。一是不断优化已经初步形成产业基础的电子信息、节能环保、高端装备制造等产业集群。二是大力鼓励珠三角和珠西经济带跨地区的企业并购活动，通过重组改造融入优势产业范畴，而不应盯着从珠三角转出的低效、高耗的小型企业。</p><p><span><b>三、处理好市场与政府的关系，提升产业合作效率</b></span></p><p>（一）发挥市场配置资源的决定性作用</p><p>处理好政府与市场、官员与企业家的关系，事关资源配置效率。珠西经济带应以粤桂合作特别试验区为切入点，改革产业合作进程中过多的行政干预，省区间合作运行机制应最大化尊重市场原则。扭转目前区域合作协调组织主体是政府的局面，鼓励企业直接进行产业合作的协调与决策。</p><p>（二）创新机制实现产业合作</p><p>克服现有行政区划障碍与解决各合作方经济发展不平衡是保证合作可持续发展的关键。这两个难题不是单纯的基于比较优势的资源配置或产业分工问题，而是要建立合作的制度基础。两省区政府需要共同创造公共服务“外部性”，通过谈判协商，尽可能在公共产品供给领域达成共识。树立风险共担、利益共享和共同管理观念。以项目为合作契机，就某项具体的基础设施建设、流域环境治理、要素流动保障、创新平台建立等项目展开合作。合作成本的分摊要根据双方从项目中受益的情况而协商，以多受益多投入、先受益先投入为原则。广东作为先发地区，亦可考虑跨省区公共设施的前期投入，以推动双方合作项目的顺利开展。</p><p><span><b>“主体功能区视角下珠江-西江经济带四化融合研究”课题</b></span></p><p><span><b>负责人：刘俊杰</b></span></p><ul><li>主 编： 林春逸、徐毅</li><li>编 辑： 熊兴华</li><li>主 办： 珠江-西江经济带发展研究院</li><li>日 期： 2016年1月1日</li><li>地 址： 广西桂林市育才路15号 广西师范大学图书馆315室</li><li>邮 编： 541004</li><li>电 话： 0773-5843330,5846433</li><li>传 真： 0773-5843330</li><li>邮 箱： skc@gxnu.edu.cn</li></ul>', '/images/upload/library/20170424/1493043479349.png', '2016-10-31 00:00:00', '2017-04-24 13:14:23', '/images/upload/library/20170424/1493014493718.pdf', '00000000000');
INSERT INTO `library` VALUES ('2', '1', 'nature14539-Deep learning', '<p>Deep learning allows computational models that are composed of multiple processing layers to learn representations of data with multiple levels of abstraction. These methods have dramatically improved the state-of-the-art in speech recognition, visual object recognition, object detection and many other domains such as drug discovery and genomics. Deep learning discovers intricate structure in large data sets by using the backpropagation algorithm to indicate how a machine should change its internal parameters that are used to compute the representation in each layer from the representation in the previous layer. Deep convolutional nets have brought about breakthroughs in processing images, video, speech and audio, whereas recurrent nets have shone light on sequential data such as text and speech.&nbsp;</p>', '/images/upload/library/20170427/1493256949962.png', '2016-11-01 00:00:00', null, '/images/upload/library/20170427/1493256955012.pdf', '00000000000');
INSERT INTO `library` VALUES ('3', '1', '口语对话系统中语音理解研究', '<p>语音识别和语义理解是口语对话系统最前端的两个模块，其性能好坏直接影响整个系统的性能。为了提高这两个模块的性能，本文提出基于语义概念的语音理解框架，并围绕语义概念在置信度确认和待登录关键词的发现和自动标 注方面提出新的方法和策略。</p><p>本文主要工作包括：&nbsp;</p><p><b>1．基于语义概念的语音理解框架。</b>针对口语对话系统中识别性能不佳的问 题，提出了以语义概念为核心的语音理解策略：在搜索中及早的利用上层语义概念知识，并将语音识别和语义理解两个步骤更加紧密地结合在一起，使得识别器可以直接输出语义概念结果。该框架利用规则描述语义概念知识，避开领域数据收集和标注的困难；另外，它具有良好的可扩展性，可以很方便地在识别中引入对话上下文知识。实验表明：在语音理解框架下，系统的识别性能和 理解性能都有较大程度的提高。&nbsp;</p><p><b>2．语义概念的置信度确认方法。</b>语义概念是整个对话系统中的核心内容， 为了减少语义概念错误对系统后续模块的负面影响，本文主要从特征方面着手，研究语义概念的置信度确认方法：针对语义概念从分析理解层面提出新的置信度特征，如概念级的语言模型得分；另外，将韵律边界信息作为一种新的特征引入到语义概念的置信度打分中。声学层、分析理解层置信特征和韵律边界特 征三者结合取得了很好的语义概念确认效果。&nbsp;</p><p><b>3. 待登录关键词的发现及其语义类属性的自动标注。</b>考虑到对话系统设计 初期关键词表不够完善的问题，提出待登录关键词发现及其语义类属性自动标注的策略：通过对语料进行语义分析发现待登录关键词，并根据上下文关系推测该词在词表中可能对应的语义类。待登录关键词发现和自动标注让系统具有一定的自学能力，可以在实际应用中使词表不断完善，使原有规则覆盖更多的 语义概念表达方式，进而提高语音理解性能。</p><p><b>&nbsp;关键词：</b>语音理解；语音确认；口语对话系统；语音识别&nbsp;</p>', '/images/upload/library/20170427/1493256714306.png', '2017-04-24 00:00:00', null, '/images/upload/library/20170427/1493256815870.pdf', '00000000000');

-- ----------------------------
-- Table structure for library_type
-- ----------------------------
DROP TABLE IF EXISTS `library_type`;
CREATE TABLE `library_type` (
  `TYPE_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '类型ID',
  `TYPE_NAME` varchar(30) DEFAULT '' COMMENT '智库类型名',
  `CREATE_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of library_type
-- ----------------------------
INSERT INTO `library_type` VALUES ('1', '智库专刊', '2017-03-15 16:47:22');
INSERT INTO `library_type` VALUES ('2', '蓝皮书', '2017-03-15 16:47:22');

-- ----------------------------
-- Table structure for login_log
-- ----------------------------
DROP TABLE IF EXISTS `login_log`;
CREATE TABLE `login_log` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '登录记录ID',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `login_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '登录时间',
  `ip_address` varchar(20) DEFAULT '' COMMENT 'IP地址',
  PRIMARY KEY (`login_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `login_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login_log
-- ----------------------------
INSERT INTO `login_log` VALUES ('1', '2', '2017-04-27 08:35:40', '116.1.3.231');
INSERT INTO `login_log` VALUES ('2', '2', '2017-04-27 08:35:40', '116.1.3.231');
INSERT INTO `login_log` VALUES ('3', '2', '2017-04-27 08:35:40', '116.1.3.231');
INSERT INTO `login_log` VALUES ('4', '2', '2017-04-27 08:35:40', '116.1.3.231');
INSERT INTO `login_log` VALUES ('5', '2', '2017-04-27 08:35:09', '0:0:0:0:0:0:0:1');
INSERT INTO `login_log` VALUES ('6', '2', '2017-04-27 09:29:29', '0:0:0:0:0:0:0:1');
INSERT INTO `login_log` VALUES ('7', '2', '2017-04-27 09:39:52', '0:0:0:0:0:0:0:1');
INSERT INTO `login_log` VALUES ('8', '1', '2017-04-27 13:53:57', '0:0:0:0:0:0:0:1');
INSERT INTO `login_log` VALUES ('9', '2', '2017-04-27 16:51:59', '0:0:0:0:0:0:0:1');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `LOGIN_NAME` varchar(20) NOT NULL DEFAULT '' COMMENT '登录名',
  `LOGIN_PWD` varchar(32) NOT NULL DEFAULT '' COMMENT '登陆密码',
  `ROLE_ID` int(11) DEFAULT '2' COMMENT '所属角色ID',
  `STATUS` int(1) DEFAULT '1' COMMENT '状态1:正常,0:冻结',
  `LAST_LOGIN_TIME` timestamp NULL DEFAULT NULL COMMENT '最后登录时间',
  `LAST_LOGIN_IP` varchar(20) DEFAULT NULL COMMENT '最后登录IP',
  `CREATE_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `EMAIL` varchar(20) DEFAULT NULL COMMENT '邮件地址',
  PRIMARY KEY (`USER_ID`),
  KEY `ROLE_ID` (`ROLE_ID`),
  CONSTRAINT `sys_user_ibfk_1` FOREIGN KEY (`ROLE_ID`) REFERENCES `user_role` (`ROLE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', '1', '2017-04-27 13:53:57', '0:0:0:0:0:0:0:1', '2016-10-30 19:54:19', '');
INSERT INTO `sys_user` VALUES ('2', 'jiyun', 'f9e11959013a3fa5692c2549156ee2ea', '1', '1', '2017-04-27 16:51:59', '0:0:0:0:0:0:0:1', '2016-11-08 21:52:58', 'accjiyun@outlook.com');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色类型ID',
  `ROLE_NAME` varchar(50) DEFAULT NULL,
  `CREATE_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', '超级管理员', '2016-12-02 08:08:06');

-- ----------------------------
-- Table structure for website_banner
-- ----------------------------
DROP TABLE IF EXISTS `website_banner`;
CREATE TABLE `website_banner` (
  `BANNER_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '轮播图ID',
  `TITLE` varchar(100) DEFAULT NULL COMMENT '轮播标题',
  `ARTICLE_URL` varchar(255) DEFAULT NULL COMMENT '文章URL',
  `BANNER_IMAGE_URL` varchar(255) DEFAULT NULL COMMENT '轮播图URL',
  `SORT` int(11) DEFAULT '1' COMMENT '排序值',
  PRIMARY KEY (`BANNER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of website_banner
-- ----------------------------
INSERT INTO `website_banner` VALUES ('1', '2015年3月习近平总书记在全国两会期间参加广西代表团审议', '/science/articleinfo/1.html', '/images/upload/carousel/b1.jpg', '1');
INSERT INTO `website_banner` VALUES ('2', '2014年8月国务院批复同意《珠江—西江经济带发展规划》', '/science/articleinfo/2.html', '/images/upload/carousel/b2.jpg', '2');
INSERT INTO `website_banner` VALUES ('3', '2014年8月国务院批复同意《珠江—西江经济带发展规划》', '/science/articleinfo/2.html', '/images/upload/carousel/b4.jpg', '3');
INSERT INTO `website_banner` VALUES ('4', '联合主办“一带一路与两岸”学术会议在桂林召开', '/science/articleinfo/8.html', '/images/upload/banner/20170424/1493041777109.jpg', '4');

-- ----------------------------
-- Table structure for website_logo
-- ----------------------------
DROP TABLE IF EXISTS `website_logo`;
CREATE TABLE `website_logo` (
  `IMAGE_ID` int(11) NOT NULL COMMENT '网站概要图片ID',
  `IMAGE_URL` varchar(255) DEFAULT NULL COMMENT '网站概要图片URL',
  PRIMARY KEY (`IMAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of website_logo
-- ----------------------------
INSERT INTO `website_logo` VALUES ('1', '/images/imgs/outline.png');
