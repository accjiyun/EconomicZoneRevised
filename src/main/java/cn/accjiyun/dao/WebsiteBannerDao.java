package cn.accjiyun.dao;

import cn.accjiyun.core.dao.BaseDao;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.website.WebsiteBanner;

import java.util.List;

/**
 * Created by jiyun on 2017/1/25.
 */
public interface WebsiteBannerDao extends BaseDao<WebsiteBanner> {
    /**
     * 添加轮播图片
     */
    public int addWebsiteBanner(WebsiteBanner websiteBanner);

    /**
     * 删除轮播图片
     */
    public void delWebsiteBanner(String[] websiteBannerIds);

    /**
     * 通过轮播图片ID查询
     *
     * @param bannerId
     * @return
     */
    public WebsiteBanner queryBannerById(int bannerId);

    /**
     * 分页查询轮播图片列表
     * @param query
     * @param pageModel
     * @return
     */
    public List<WebsiteBanner> queryBannerPage(QueryArticle query, PageModel<WebsiteBanner> pageModel);

    /**
     * 更新轮播图片
     */
    public void updateWebsiteBanner(WebsiteBanner websiteBanner);

    /**
     * 获取所有轮播图片总记录数
     *
     * @return 总记录数
     */
    public long queryAllBannerCount();
}
