package cn.accjiyun.dao;

import cn.accjiyun.core.dao.BaseDao;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.model.website.WebsiteLogo;

import java.util.List;

/**
 * Created by jiyun on 2017/2/10.
 */
public interface WebsiteLogoDao extends BaseDao<WebsiteLogo> {
    /**
     * 添加网站Logo
     *
     * @param websiteLogo
     */
    public int addWebsiteLogo(WebsiteLogo websiteLogo);

    /**
     * 删除网站Logo
     */
    public void deleteWebsiteLogo(String[] websiteLogoIds);

    /**
     * 通过轮播图ID查询
     * @param imageId
     * @return
     */
    public WebsiteLogo queryWebsiteLogoById(int imageId);

    /**
     * 分页查询网站Logo列表
     *
     * @param pageModel 分页条件
     * @return List<WebsiteLogo>网站Logo列表
     */
    public List<WebsiteLogo> queryWebsiteLogoPage(PageModel<WebsiteLogo> pageModel);

    /**
     * 更新网站重磅发布图
     * @param websiteLogo
     */
    public void updateWebsiteLogo(WebsiteLogo websiteLogo);

    /**
     * 获取所有总记录数
     *
     * @return 总记录数
     */
    public long queryAllCount();
}
