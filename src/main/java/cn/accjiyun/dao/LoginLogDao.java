package cn.accjiyun.dao;

import cn.accjiyun.core.dao.BaseDao;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.system.LoginLog;

import java.util.List;

/**
 * Created by jiyun on 2017/4/25.
 */
public interface LoginLogDao extends BaseDao<LoginLog> {

    /**
     * 添加登录日志
     * @param loginLog
     * @return
     */
    public int createLoginLog(LoginLog loginLog);

    /**
     * 分页查询登录日志
     * @param query
     * @param model
     * @return
     */
    public List<LoginLog> queryLoginLogPage(QueryArticle query, PageModel<LoginLog> model);
}
