package cn.accjiyun.dao.impl;

import cn.accjiyun.core.dao.DaoSupport;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.dao.SysUserDao;
import cn.accjiyun.model.system.QuerySysUser;
import cn.accjiyun.model.system.SysUser;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2016/11/15.
 */
@Repository("sysUserDao")
public class SysUserDaoImpl extends DaoSupport<SysUser> implements SysUserDao {

    /**
     * 创建用户
     *
     * @param sysuser 用户实体
     * @return 用户ID
     */
    @Override
    public int createSysUser(SysUser sysuser) {
        save(sysuser);
        return sysuser.getUserId();
    }

    /**
     * 更新用户信息
     *
     * @param sysuser 用户实体
     */
    @Override
    public void updateSysUser(SysUser sysuser) {
        update(sysuser);
    }

    /**
     * 通过ID，查询用户实体信息
     *
     * @param userId 用户ID
     * @return SysUser
     */
    @Override
    public SysUser querySysUserByUserId(int userId) {
        return get(userId);
    }

    /**
     * 分页查询用户列表
     *
     * @param querySysUser 查询条件
     * @param pageModel    分页条件
     * @return 用户实体列表
     */
    @Override
    public List<SysUser> querySysUserPage(QuerySysUser querySysUser, PageModel<SysUser> pageModel) {
        String where = new StringBuffer().append("where ")
                .append(querySysUser.getRoleId() == 0 ? "" : "roleId=?").toString();
        Object[] params = {querySysUser.getRoleId()};
        if (querySysUser.getRoleId() == 0) params = null;
        Map<String, String> orderby = new HashMap<>();
        orderby.put("lastLoginTime", "desc");
        PageModel<SysUser> result = find(where, params, orderby, pageModel.getPageNo(), pageModel.getPageSize());
        pageModel.setTotalRecords(result.getTotalRecords());
        pageModel.setTotalPageSize(result.getTotalPageSize());
        return result.getList();
    }

    /**
     * 验证用户帐户是否存在
     *
     * @param userLoginName
     * @return
     */
    @Override
    public int validateLoginName(String userLoginName) {
        String hql = "select count(*) from SysUser where loginName=?";
        Object[] params = {userLoginName};
        int number = uniqueResult(hql, params) == null ? 0 : 1;
        return number;
    }

    /**
     * 查询登录用户
     *
     * @param sysUser 查询条件
     * @return SysUser
     */
    @Override
    public SysUser queryLoginUser(SysUser sysUser) {
        String hql = "from SysUser where loginName=? and loginPwd=?";
        Object[] params = {sysUser.getLoginName(), sysUser.getLoginPwd()};
        return (SysUser) uniqueResult(hql, params);
    }

    /**
     * 修改用户密码
     *
     * @param userId   用户ID
     * @param loginPwd
     */
    @Override
    public void updateUserPwd(int userId, String loginPwd) {
        SysUser sysUser = querySysUserByUserId(userId);
        sysUser.setLoginPwd(loginPwd);
        update(sysUser);
    }

    /**
     * 禁用或启用后台用户
     *
     * @param userId 用户ID
     * @param status 状态0:正常,1:冻结
     */
    @Override
    public void updateDisableOrstartUser(int userId, int status) {
        SysUser sysUser = querySysUserByUserId(userId);
        sysUser.setStatus(status);
        update(sysUser);
    }


    /***
     * 修改用户登录最后登录时间和IP

     * @param map key: userId, time, ip.
     */
    @Override
    public void updateUserLoginLog(Map<String, Object> map) {
        SysUser sysUser = querySysUserByUserId((int) map.get("userId"));
        sysUser.setLastLoginTime(new Timestamp(((Date) map.get("time")).getTime()));
        sysUser.setLastLoginIp((String) map.get("ip"));
    }

    /**
     * 登陆
     *
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @Override
    public SysUser login(String username, String password) {
        if (username != null && password != null) {
            String where = "where loginName=? and loginPwd=?";
            Object[] queryParams = {username, password};
            List<SysUser> list = find(-1, -1, where, queryParams).getList();
            if (list != null && list.size() > 0) {
                return list.get(0);
            }
        }
        return null;
    }
}
