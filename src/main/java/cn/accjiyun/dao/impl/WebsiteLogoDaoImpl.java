package cn.accjiyun.dao.impl;

import cn.accjiyun.core.dao.DaoSupport;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.dao.WebsiteLogoDao;
import cn.accjiyun.model.website.WebsiteLogo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/2/10.
 */
@Repository("websiteLogoDao")
public class WebsiteLogoDaoImpl extends DaoSupport<WebsiteLogo> implements WebsiteLogoDao {
    /**
     * 添加网站Logo
     *
     * @param websiteLogo
     */
    @Override
    public int addWebsiteLogo(WebsiteLogo websiteLogo) {
        save(websiteLogo);
        return websiteLogo.getImageId();
    }

    /**
     * 删除网站Logo
     */
    @Override
    public void deleteWebsiteLogo(String[] websiteLogoIds) {
        for (int i = 0; i < websiteLogoIds.length; i++) {
            delete(Integer.valueOf(websiteLogoIds[i]));
        }
    }

    /**
     * 通过轮播图ID查询
     *
     * @param imageId
     * @return
     */
    @Override
    public WebsiteLogo queryWebsiteLogoById(int imageId) {
        return get(imageId);
    }

    /**
     * 分页查询网站Logo列表
     *
     * @param pageModel 分页条件
     * @return List<WebsiteLogo>网站Logo列表
     */
    @Override
    public List<WebsiteLogo> queryWebsiteLogoPage(PageModel<WebsiteLogo> pageModel) {
        PageModel<WebsiteLogo> result = find(pageModel.getPageNo(), pageModel.getPageSize());
        pageModel.setTotalRecords(result.getTotalRecords());
        pageModel.setTotalPageSize(result.getTotalPageSize());
        return result.getList();
    }

    /**
     * 更新网站重磅发布图
     *
     * @param websiteLogo
     */
    @Override
    public void updateWebsiteLogo(WebsiteLogo websiteLogo) {
        saveOrUpdate(websiteLogo);
    }

    /**
     * 获取所有总记录数
     *
     * @return 总记录数
     */
    @Override
    public long queryAllCount() {
        return getCount();
    }
}
