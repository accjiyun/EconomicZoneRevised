package cn.accjiyun.dao.impl;

import cn.accjiyun.core.dao.DaoSupport;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.core.utils.WebUtils;
import cn.accjiyun.dao.LibraryDao;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.library.Library;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2017/2/10.
 */
@Repository("libraryDaoImpl")
public class LibraryDaoImpl extends DaoSupport<Library> implements LibraryDao {

    @Override
    public int createLibrary(Library library) {
        save(library);
        return library.getLibraryId();
    }

    @Override
    public void updateLibrary(Library library) {
        saveOrUpdate(library);
    }

    @Override
    public void deleteLibraryByIds(String[] libraryIds) {
        for (int i = 0; i < libraryIds.length; i++) {
            delete(Integer.valueOf(libraryIds[i]));
        }
    }

    @Override
    public Library queryLibraryById(int libraryId) {
        return get(libraryId);
    }

    @Override
    public List<Library> queryLibraryPage(QueryArticle query, PageModel<Library> pageModel) {
        Timestamp beginCreateTime = query.getBeginCreateTime() == null ?
                null : new Timestamp(query.getBeginCreateTime().getTime());
        Timestamp endCreateTime = query.getEndCreateTime() == null ?
                null : new Timestamp(query.getEndCreateTime().getTime());
        String where = new StringBuffer().append("where 1=1")
                .append(query.getType() == 0 ? "" : " and libraryType=?")
                .append(!WebUtils.isValidateRealString(query.getQueryKey()) ? "" : " and title like '%" + query.getQueryKey() + "%'")
                .append(beginCreateTime == null ? "" : " and publishTime >= ?")
                .append(endCreateTime == null ? "" : " and publishTime <= ?")
                .toString();
        Object[] params = {query.getType(), beginCreateTime, endCreateTime};
        if (query.getType() == 0) params[0] = null;
        Map<String, String> orderBy = new HashMap<>();
        orderBy.put("publishTime", "desc");
        PageModel<Library> result;
        if (query.getType() == 0 && query.getQueryKey() == null) {
            result = find(pageModel.getPageNo(), pageModel.getPageSize(), orderBy);
        } else {
            result = find(where, params, orderBy, pageModel.getPageNo(), pageModel.getPageSize());
        }
        pageModel.setTotalRecords(result.getTotalRecords());
        pageModel.setTotalPageSize(result.getTotalPageSize());
        return result.getList();
    }

    @Override
    public void updateDownloadNum(Map<String, String> map) {
        Library library = get(queryLibraryById(Integer.valueOf(map.get("libraryId"))));
        if (map.get("num").equals("+1")) {
            library.setDownloadNum(library.getDownloadNum() + 1);
        } else {
            library.setDownloadNum(Integer.valueOf(map.get("num")));
        }
    }

    @Override
    public long queryAllLibraryCount() {
        return getCount();
    }
}
