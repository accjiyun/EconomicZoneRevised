package cn.accjiyun.dao.impl;

import cn.accjiyun.core.dao.DaoSupport;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.dao.WebsiteBannerDao;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.website.WebsiteBanner;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2017/2/10.
 */
@Repository("websiteBannerDao")
public class WebsiteBannerDaoImpl extends DaoSupport<WebsiteBanner> implements WebsiteBannerDao {

    /**
     * 添加轮播图片
     *
     * @param websiteBanner
     */
    @Override
    public int addWebsiteBanner(WebsiteBanner websiteBanner) {
        save(websiteBanner);
        return websiteBanner.getBannerId();
    }

    /**
     * 删除轮播图片
     *
     * @param websiteBannerIds
     */
    @Override
    public void delWebsiteBanner(String[] websiteBannerIds) {
        for (int i = 0; i < websiteBannerIds.length; i++) {
            delete(Integer.valueOf(websiteBannerIds[i]));
        }
    }

    /**
     * 通过文章ID查询
     *
     * @param bannerId
     * @return
     */
    @Override
    public WebsiteBanner queryBannerById(int bannerId) {
        return get(bannerId);
    }

    /**
     * 分页查询轮播图片列表
     * @param query
     * @param pageModel
     * @return
     */
    @Override
    public List<WebsiteBanner> queryBannerPage(QueryArticle query, PageModel<WebsiteBanner> pageModel) {
        Map<String, String> orderBy = new HashMap<>();
        orderBy.put("sort", "asc");
        PageModel<WebsiteBanner> result = find(pageModel.getPageNo(), pageModel.getPageSize(), orderBy);
        pageModel.setTotalRecords(result.getTotalRecords());
        pageModel.setPageSize(result.getPageSize());
        return result.getList();
    }

    /**
     * 更新轮播图片
     *
     * @param websiteBanner
     */
    @Override
    public void updateWebsiteBanner(WebsiteBanner websiteBanner) {
        saveOrUpdate(websiteBanner);
    }

    /**
     * 获取所有轮播图片总记录数
     *
     * @return 总记录数
     */
    @Override
    public long queryAllBannerCount() {
        return getCount();
    }
}
