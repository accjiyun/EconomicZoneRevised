package cn.accjiyun.dao.impl;

import cn.accjiyun.core.dao.DaoSupport;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.core.utils.WebUtils;
import cn.accjiyun.dao.ArticleDao;
import cn.accjiyun.model.article.Article;
import cn.accjiyun.model.article.QueryArticle;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2016/11/13.
 */
@Repository("articleDao")
public class ArticleDaoImpl extends DaoSupport<Article> implements ArticleDao {
    /**
     * 创建文章
     *
     * @param article 文章实体
     * @return 返回文章ID
     */
    @Override
    public int createArticle(Article article) {
        save(article);
        return article.getArticleId();
    }

    /**
     * 修改文章信息
     *
     * @param article 文章实体
     */
    @Override
    public void updateArticle(Article article) {
        saveOrUpdate(article);
    }

    /**
     * 删除文章
     *
     * @param articleIds 文章ID
     */
    @Override
    public void deleteArticleByIds(String[] articleIds) {
        for (int i = 0; i < articleIds.length; i++) {
            delete(Integer.valueOf(articleIds[i]));
        }
    }

    /**
     * 通过文章ID查询文章信息
     *
     * @param articleId 文章ID
     * @return Article文章实体信息
     */
    @Override
    public Article queryArticleById(int articleId) {
        return get(articleId);
    }

    /**
     * 分页查询文章列表
     *
     * @param query     查询条件
     * @param pageModel 分页条件
     * @return List<Article>文章列表
     */
    @Override
    public List<Article> queryArticlePage(QueryArticle query, PageModel<Article> pageModel) {
        Timestamp beginCreateTime = query.getBeginCreateTime() == null ?
                null : new Timestamp(query.getBeginCreateTime().getTime());
        Timestamp endCreateTime = query.getEndCreateTime() == null ?
                null : new Timestamp(query.getEndCreateTime().getTime());
        String where = new StringBuffer().append("where 1=1")
                .append(query.getStatus() == 3 ? "" : " and status=?")
                .append(query.getType() == 0 ? "" : " and articleType=?")
                .append(!WebUtils.isValidateRealString(query.getQueryKey()) ? "" :
                        " and (title like '%" + query.getQueryKey() + "%' or keyWord like '%" + query.getQueryKey() + "%' )")
                .append(beginCreateTime == null ? "" : " and publishTime >= ?")
                .append(endCreateTime == null ? "" : " and publishTime <= ?")
                .toString();
        Object[] params = {query.getStatus(), query.getType(), beginCreateTime, endCreateTime};
        if (query.getStatus() == 3) params[0] = null;
        if (query.getType() == 0) params[1] = null;
        Map<String, String> orderBy = new HashMap<>();
        orderBy.put("publishTime", "desc");
        PageModel<Article> result;
        if (query.getType() == 0 && query.getQueryKey() == null) {
            result = find(pageModel.getPageNo(), pageModel.getPageSize(), orderBy);
        } else {
            result = find(where, params, orderBy, pageModel.getPageNo(), pageModel.getPageSize());
        }
        pageModel.setTotalRecords(result.getTotalRecords());
        pageModel.setTotalPageSize(result.getTotalPageSize());
        return result.getList();
    }

    /**
     * 修改累加文章点击量
     *
     * @param map
     */
    @Override
    public void updateArticleNum(Map<String, String> map) {
        Article article = get(Integer.valueOf(map.get("articleId")));
        if (map.get("num").equals("+1")) {
            article.setClickNum(article.getClickNum() + 1);
        } else {
            article.setClickNum(Integer.valueOf(map.get("num")));
        }
    }

    /**
     * 获取所有文章总记录数
     *
     * @return 总记录数
     */
    @Override
    public int queryAllArticleCount() {
        return (int) getCount();
    }

    /**
     * 利用hql语句查找信息
     *
     * @param hql
     * @param queryParams
     * @return
     */
    public List<Article> CreateQuery(final String hql, final Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);//设置查询参数
        return query.list();
    }
}
