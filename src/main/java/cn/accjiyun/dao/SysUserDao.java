package cn.accjiyun.dao;

import cn.accjiyun.core.dao.BaseDao;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.model.system.QuerySysUser;
import cn.accjiyun.model.system.SysUser;

import java.util.List;
import java.util.Map;

public interface SysUserDao extends BaseDao<SysUser> {
    public int createSysUser(SysUser sysuser);

    public void updateSysUser(SysUser sysuser);

    public SysUser querySysUserByUserId(int userId);

    public List<SysUser> querySysUserPage(QuerySysUser querySysUser, PageModel<SysUser> pageModel);

    public int validateLoginName(String userLoginName);

    public SysUser queryLoginUser(SysUser sysUser);

    public void updateUserPwd(int userId, String loginPwd);

    public void updateDisableOrstartUser(int userId, int status);

    public void updateUserLoginLog(Map<String, Object> map);

    public SysUser login(String username, String password);
}
