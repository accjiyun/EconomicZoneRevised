package cn.accjiyun.controller.website;

import cn.accjiyun.core.controller.BaseController;
import cn.accjiyun.model.website.WebsiteLogo;
import cn.accjiyun.service.WebsiteLogoService;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 * Created by jiyun on 2017/2/11.
 */
@Controller
@RequestMapping("/admin/websiteInfo")
public class AdminInfoController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(AdminInfoController.class);
    private static String infoPage = getViewPath("/admin/website/website-info");

    @Autowired
    private WebsiteLogoService websiteLogoService;

    @RequestMapping("/updateWebsiteInfo")
    @ResponseBody
    public String updateWebsiteInfo(HttpServletRequest request) {
        JsonObject resultJson = new JsonObject();
        try {
            String imageUrl = request.getParameter("articleUrl");
            WebsiteLogo websiteLogo = websiteLogoService.getWebsiteLogo();
            websiteLogo.setImageUrl(imageUrl);
            websiteLogoService.setWebsiteLogo(websiteLogo);
            resultJson.addProperty("status", 200);
            resultJson.addProperty("msg", "修改成功！");
        } catch (Exception e) {
            logger.error("AdminInfoController.updateWebsiteInfo()--error", e);
            resultJson.addProperty("status", 400);
            resultJson.addProperty("msg", "更新失败！");
        }
        return gson.toJson(resultJson);
    }

    @RequestMapping(value = "/upload")
    public String upload(@RequestParam(value = "file", required = false) MultipartFile file,
                         HttpServletRequest request, ModelMap model) {
        String fileName = file.getOriginalFilename();
        String logoRealPathDir = request.getSession().getServletContext()
                .getRealPath("images/upload/webInfo");
        File targetFile = new File(logoRealPathDir, fileName);
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        try {
            file.transferTo(targetFile);
            logger.info("Upload success to : " + targetFile.getAbsolutePath());
        } catch (Exception e) {
            logger.error("upload()--error", e);
            e.printStackTrace();
        }
        model.addAttribute("fileUrl", request.getContextPath() + "/upload/" + fileName);
        return "redirect:/admin/websiteInfo/initInfoPage";
    }


    /**
     * 初始化网站信息修改页面
     */
    @RequestMapping("/initInfoPage")
    public ModelAndView initInfoPage() {
        ModelAndView model = new ModelAndView();
        model.setViewName(infoPage);
        model.addObject("imageUrl", websiteLogoService.getWebsiteLogo().getImageUrl());
        return model;
    }

}
