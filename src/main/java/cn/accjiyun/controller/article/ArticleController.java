package cn.accjiyun.controller.article;

import cn.accjiyun.core.controller.BaseController;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.model.article.Article;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.service.ArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2017/2/3.
 */
@Controller
public class ArticleController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(ArticleController.class);
    // 文章列表
    private static String listPage = getViewPath("/web/science/article-list");
    // 文章详情
    private static String articleInfo = getViewPath("/web/science/article-info");

    @Autowired
    private ArticleService articleService;

    /**
     * 分页查询文章列表
     */
    @RequestMapping("/science/{articleType}")
    public ModelAndView showArticleList(HttpServletRequest request, @ModelAttribute("queryArticle") QueryArticle queryArticle,
                                        @ModelAttribute("page") PageModel<Article> page, @PathVariable("articleType") int articleType) {
        ModelAndView model = new ModelAndView();
        try {
            if (articleType <= 3 && articleType > 0) {
                // 查询已经发布的文章资讯
                queryArticle.setType(articleType);
                queryArticle.setStatus(1);
                page.setPageSize(10);
                List<Article> articleList = articleService.queryArticlePage(queryArticle, page);
                model.addObject("articleList", articleList);
                model.addObject("page", page);
                model.addObject("articleType", articleType);
                model.setViewName(listPage);
            } else {
                model.setViewName("common/404");
                return model;
            }
        } catch (Exception e) {
            model.setViewName(this.setExceptionRequest(request, e));
            logger.error("showArticleList()--error", e);
        }
        return model;
    }

    @RequestMapping("/science")
    public ModelAndView showArticleList(HttpServletRequest request) {
        return new ModelAndView("redirect:/science/0");
    }

    /**
     * 修改文章点击数量
     */
    @RequestMapping("/updateArticleClickNum/{articleId}")
    @ResponseBody
    public Map<String, Object> updateArticleClickNum(HttpServletRequest request, @PathVariable("articleId") int articleId) {
        Map<String, Object> json = new HashMap<>();
        try {
            Map<String, String> map = new HashMap<>();
            map.put("num", "+1");
            map.put("type", "clickNum");
            map.put("articleId", articleId + "");
            articleService.updateArticleNum(map);
            Article article = articleService.queryArticleById(articleId);
            json = this.setJson(true, null, article);
        } catch (Exception e) {
            this.setAjaxException(json);
            logger.error("updateArticleClickNum()--error", e);
        }
        return json;
    }

    /**
     * 文章详情
     */
    @RequestMapping("/science/articleinfo/{id}.html")
    public String articleInfo(HttpServletRequest request, @PathVariable("id") int id) {
        try {
            // 查询文章详情
            Article article = articleService.queryArticleById(id);
            String content = articleService.queryArticleContentByArticleId(id);
            request.setAttribute("content", content);
            request.setAttribute("article", article);
            Map<String, String> map = new HashMap<>();
            map.put("num", "+1");
            map.put("type", "clickNum");
            map.put("articleId", article.getArticleId() + "");
            articleService.updateArticleNum(map);
        } catch (Exception e) {
            logger.error("articleInfo()--error", e);
            return this.setExceptionRequest(request, e);
        }
        return articleInfo;
    }

}
