package cn.accjiyun.controller.webfront;

import cn.accjiyun.core.controller.BaseController;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.core.utils.WebUtils;
import cn.accjiyun.model.article.Article;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.library.Library;
import cn.accjiyun.model.website.WebsiteBanner;
import cn.accjiyun.service.ArticleService;
import cn.accjiyun.service.LibraryService;
import cn.accjiyun.service.WebsiteBannerService;
import cn.accjiyun.service.WebsiteLogoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by jiyun on 2017/2/1.
 */
@Controller
public class WebFrontController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(WebFrontController.class);

    @Autowired
    private ArticleService articleService;
    @Autowired
    private WebsiteBannerService bannerService;
    @Autowired
    private WebsiteLogoService logoService;
    @Autowired
    private LibraryService libraryService;

    /**
     * 首页获取网站首页数据
     */
    @RequestMapping("/index")
    public String getIndexpage(HttpServletRequest request, Model model) {
        try {
            PageModel<WebsiteBanner> bannerPageModel = new PageModel<>();
            bannerPageModel.setPageSize((int) bannerService.queryAllBannerCount());
            List<WebsiteBanner> bannerList = bannerService.queryBannnerPage(new QueryArticle(), bannerPageModel);
            model.addAttribute("bannerList", bannerList);

            model.addAttribute("websiteLogo", logoService.getWebsiteLogo());

            PageModel<Article> pageModel = new PageModel<>();
            pageModel.setPageSize(4);
            pageModel.setPageNo(1);

            QueryArticle queryNews = new QueryArticle();
            queryNews.setType(1);
            queryNews.setStatus(1);
            List<Article> newsList = articleService.queryArticlePage(queryNews, pageModel);
            model.addAttribute("newsList", newsList);

            QueryArticle queryActivities = new QueryArticle();
            queryActivities.setType(2);
            queryActivities.setStatus(1);
            List<Article> activitiesList = articleService.queryArticlePage(queryActivities, pageModel);
            model.addAttribute("activitiesList", activitiesList);

            QueryArticle queryNotices = new QueryArticle();
            queryNotices.setType(3);
            queryNotices.setStatus(1);
            List<Article> noticesList = articleService.queryArticlePage(queryNotices, pageModel);
            model.addAttribute("noticesList", noticesList);

            QueryArticle queryLibrary = new QueryArticle();
            queryLibrary.setType(1);
            PageModel<Library> libraryPageModel = new PageModel<>();
            libraryPageModel.setPageNo(1);
            libraryPageModel.setPageSize(1);
            Library library = libraryService.queryLibraryPage(queryLibrary, libraryPageModel).get(0);
            model.addAttribute("library", library);
            String libraryText = WebUtils.html2Text(library.getSummary());
            if (libraryText.length() > 190) {
                libraryText = libraryText.substring(0, 190) + "...";
            }
            model.addAttribute("libraryText", libraryText);
        } catch (Exception e) {
            logger.error("WebFrontController.getIndexpage", e);
            return setExceptionRequest(request, e);
        }
        return getViewPath("/web/front/index");
    }


}
