package cn.accjiyun.controller.library;

import cn.accjiyun.core.controller.BaseController;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.model.article.Article;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.library.Library;
import cn.accjiyun.service.ArticleService;
import cn.accjiyun.service.LibraryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Created by jiyun on 2017/2/11.
 */
@Controller
@RequestMapping("/library")
public class LibraryController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(LibraryController.class);
    // 信息简报
    private static String listBriefPage = getViewPath("/web/library/brief-list");
    // 信息简报
    private static String briefInfo = getViewPath("/web/library/brief-info");

    private static String listLibraryPage = getViewPath("/web/library/library-list");

    private static String libraryInfo = getViewPath("/web/library/library-info");

    @Autowired
    private ArticleService articleService;
    @Autowired
    private LibraryService libraryService;

    /**
     * 分页查询信息简报列表
     */
    @RequestMapping("/{type}")
    public ModelAndView showArticleList(HttpServletRequest request, @ModelAttribute("queryArticle") QueryArticle queryArticle,
                                        @ModelAttribute("page") PageModel page, @PathVariable("type") int type) {
        ModelAndView model = new ModelAndView();
        try {
            // 查询已经发布的文章资讯
            queryArticle.setType(type);
            page.setPageSize(10);
            if (type == 4) {
                queryArticle.setStatus(1);
                List<Article> articleList = articleService.queryArticlePage(queryArticle, page);
                model.addObject("articleList", articleList);
                model.addObject("page", page);
                model.addObject("articleType", type);
                model.setViewName(listBriefPage);
            } else if (type == 1 || type == 2) {
                List<Library> libraryList = libraryService.queryLibraryPage(queryArticle, page);
                model.addObject("libraryList", libraryList);
                model.addObject("page", page);
                model.addObject("libraryType", type);
                model.setViewName(listLibraryPage);
                return model;
            } else {
                model.setViewName("common/404");
                return model;
            }
        } catch (Exception e) {
            model.setViewName(this.setExceptionRequest(request, e));
            logger.error("showArticleList()--error", e);
        }
        return model;
    }

    /**
     * 智库详情
     */
    @RequestMapping("/library/{id}.html")
    public String libraryInfo(HttpServletRequest request, @PathVariable("id") int id) {
        try {
            // 查询智库详情
            Library library = libraryService.queryLibraryById(id);
            request.setAttribute("library", library);
        } catch (Exception e) {
            logger.error("libraryInfo()--error", e);
            return this.setExceptionRequest(request, e);
        }
        return libraryInfo;
    }

    @RequestMapping
    public ModelAndView showArticleList(HttpServletRequest request) {
        return new ModelAndView("redirect:/library/4");
    }

    /**
     * 信息简报详情
     */
    @RequestMapping("/briefinfo/{id}.html")
    public String articleInfo(HttpServletRequest request, @PathVariable("id") int id) {
        try {
            // 查询信息简报详情
            Article article = articleService.queryArticleById(id);
            String content = articleService.queryArticleContentByArticleId(id);
            request.setAttribute("content", content);
            request.setAttribute("article", article);
        } catch (Exception e) {
            logger.error("articleInfo()--error", e);
            return this.setExceptionRequest(request, e);
        }
        return briefInfo;
    }

}
