package cn.accjiyun.controller.search;

import cn.accjiyun.core.controller.BaseController;
import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.core.utils.WebUtils;
import cn.accjiyun.model.article.Article;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.library.Library;
import cn.accjiyun.service.ArticleService;
import cn.accjiyun.service.LibraryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by jiyun on 2017/4/27.
 */
@Controller
public class GlobalSearch extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(GlobalSearch.class);
    private static String resultsPage = getViewPath("/web/front/search");

    @Autowired
    private ArticleService articleService;
    @Autowired
    private LibraryService libraryService;

    @RequestMapping("/showSearchList")
    public ModelAndView showSearchList(HttpServletRequest request, @ModelAttribute("queryArticle") QueryArticle queryArticle,
                                       @ModelAttribute("page") PageModel page, String keyword) {
        ModelAndView model = new ModelAndView();
        model.setViewName(resultsPage);
        try {
            if (WebUtils.isValidateRealString(keyword)) {
                List<Article> articleList = articleService.queryArticlePage(queryArticle, page);
                List<Library> libraryList = libraryService.queryLibraryPage(queryArticle, page);
                model.addObject("articleList", articleList);
                model.addObject("libraryList", libraryList);
                model.addObject("page", page);
            }
        } catch (Exception e) {
            model.setViewName(this.setExceptionRequest(request, e));
            logger.error("GlobalSearch.showSearchList()--error", e);
        }
        return model;
    }

}
