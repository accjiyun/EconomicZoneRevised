package cn.accjiyun.core.converter;

import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;

/**
 * Created by jiyun on 4/16/2017.
 */
public class TimestampConverter implements Converter<String, Timestamp> {

    @Override
    public Timestamp convert(String timeStr) {
        Timestamp t = null;
        if (timeStr != null && !timeStr.equals("")) {
            long time = Long.valueOf(timeStr);
            t = new Timestamp(time);
        }
        return t;
    }


}