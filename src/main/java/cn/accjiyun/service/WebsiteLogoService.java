package cn.accjiyun.service;

import cn.accjiyun.model.website.WebsiteLogo;

import java.util.List;

/**
 * Created by jiyun on 2017/2/10.
 */
public interface WebsiteLogoService {

    public List<WebsiteLogo> queryAllWebsiteLogo();

    public WebsiteLogo getWebsiteLogo();

    public void setWebsiteLogo(WebsiteLogo websiteLogo);
}
