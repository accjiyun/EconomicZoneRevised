package cn.accjiyun.service;

import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.library.Library;

import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2017/2/10.
 */
public interface LibraryService {

    public int createLibrary(Library library);

    public void updateLibrary(Library library);

    public void deleteLibraryByIds(String[] libraryIds);

    public Library queryLibraryById(int libraryId);

    public List<Library> queryLibraryPage(QueryArticle query, PageModel<Library> pageModel);

    public void updateDownloadNum(Map<String, String> map);

    public int queryAllLibraryCount();
}
