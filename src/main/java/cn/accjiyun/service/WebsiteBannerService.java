package cn.accjiyun.service;

import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.website.WebsiteBanner;

import java.util.List;

/**
 * Created by jiyun on 2017/2/10.
 */
public interface WebsiteBannerService {
    public int create(WebsiteBanner banner);

    public void updateWebsiteBanner(WebsiteBanner banner);

    public void deleteWebsiteBannerByIds(String[] bannerIds);

    public WebsiteBanner queryWebsiteBannerById(int bannerId);

    public List<WebsiteBanner> queryBannnerPage(QueryArticle query, PageModel<WebsiteBanner> pageModel);

    /**
     * 获取所有文章总记录数
     *
     * @return 总记录数
     */
    public long queryAllBannerCount();
}
