package cn.accjiyun.service.impl;

import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.dao.LoginLogDao;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.system.LoginLog;
import cn.accjiyun.service.LoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiyun on 2017/4/25.
 */
@Service("loginLogService")
public class LoginLogServiceImpl implements LoginLogService{

    @Autowired
    private LoginLogDao loginLogDao;

    /**
     * 添加登录日志
     *
     * @param loginLog
     * @return
     */
    @Override
    public int createLoginLog(LoginLog loginLog) {
        return loginLogDao.createLoginLog(loginLog);
    }

    /**
     * 分页查询登录日志
     *
     * @param query
     * @param model
     * @return
     */
    @Override
    public List<LoginLog> queryLoginLogPage(QueryArticle query, PageModel<LoginLog> model) {
        return loginLogDao.queryLoginLogPage(query, model);
    }
}
