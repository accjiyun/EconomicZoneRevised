package cn.accjiyun.service.impl;

import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.dao.WebsiteBannerDao;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.website.WebsiteBanner;
import cn.accjiyun.service.WebsiteBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiyun on 2017/2/10.
 */
@Service("websiteBannerService")
public class WebsiteBannerServiceImpl implements WebsiteBannerService {

    @Autowired
    private WebsiteBannerDao websiteBannerDao;

    @Override
    public int create(WebsiteBanner banner) {
        return websiteBannerDao.addWebsiteBanner(banner);
    }

    @Override
    public void updateWebsiteBanner(WebsiteBanner banner) {
        websiteBannerDao.updateWebsiteBanner(banner);
    }

    @Override
    public void deleteWebsiteBannerByIds(String[] bannerIds) {
        if (bannerIds != null && bannerIds.length > 0) {
            websiteBannerDao.delete(bannerIds);
        }
    }

    @Override
    public WebsiteBanner queryWebsiteBannerById(int bannerId) {
        return websiteBannerDao.get(bannerId);
    }

    @Override
    public List<WebsiteBanner> queryBannnerPage(QueryArticle query, PageModel<WebsiteBanner> pageModel) {
        return websiteBannerDao.queryBannerPage(query, pageModel);
    }

    /**
     * 获取所有文章总记录数
     *
     * @return 总记录数
     */
    @Override
    public long queryAllBannerCount() {
        return websiteBannerDao.getCount();
    }

}
