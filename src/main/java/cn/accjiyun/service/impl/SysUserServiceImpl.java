package cn.accjiyun.service.impl;

import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.dao.SysUserDao;
import cn.accjiyun.model.system.QuerySysUser;
import cn.accjiyun.model.system.SysUser;
import cn.accjiyun.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2016/12/4.
 */
@Service("sysUserService")
public class SysUserServiceImpl implements SysUserService {
    @Autowired
    private SysUserDao sysUserDao;


    public int createSysUser(SysUser sysuser) {
        return sysUserDao.createSysUser(sysuser);
    }


    public void updateSysUser(SysUser sysuser) {
        sysUserDao.updateSysUser(sysuser);
    }


    public SysUser querySysUserByUserId(int userId) {
        return sysUserDao.querySysUserByUserId(userId);
    }


    public List<SysUser> querySysUserPage(QuerySysUser querySysUser,
                                          PageModel<SysUser> page) {
        return sysUserDao.querySysUserPage(querySysUser, page);
    }


    public boolean validateLoginName(String userLoginName) {
        int count = sysUserDao.validateLoginName(userLoginName);
        if (count <= 0) {
            return true;
        }
        return false;
    }


    public SysUser queryLoginUser(SysUser sysUser) {
        return sysUserDao.queryLoginUser(sysUser);
    }


    public void updateUserPwd(SysUser sysUser) {
        sysUserDao.updateUserPwd(sysUser.getUserId(), sysUser.getLoginPwd());
    }


    public void updateDisableOrstartUser(int userId, int type) {
        sysUserDao.updateDisableOrstartUser(userId, type);
    }


    public void updateUserLoginLog(int userId, Date time, String ip) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("userId", userId);
        map.put("time", time);
        map.put("ip", ip);
        sysUserDao.updateUserLoginLog(map);
    }

    /**
     * 登陆
     *
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @Override
    public SysUser login(String username, String password) {
        return sysUserDao.login(username, password);
    }

}
