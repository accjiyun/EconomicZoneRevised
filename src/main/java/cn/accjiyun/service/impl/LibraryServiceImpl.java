package cn.accjiyun.service.impl;

import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.dao.LibraryDao;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.model.library.Library;
import cn.accjiyun.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2017/2/10.
 */
@Service("libraryService")
public class LibraryServiceImpl implements LibraryService {

    @Autowired
    private LibraryDao libraryDao;

    @Override
    public int createLibrary(Library library) {
        return libraryDao.createLibrary(library);
    }

    @Override
    public void updateLibrary(Library library) {
        libraryDao.updateLibrary(library);
    }

    @Override
    public void deleteLibraryByIds(String[] libraryIds) {
        if (libraryIds != null && libraryIds.length > 0) {
            libraryDao.deleteLibraryByIds(libraryIds);
        }
    }

    @Override
    public Library queryLibraryById(int libraryId) {
        return libraryDao.queryLibraryById(libraryId);
    }

    @Override
    public List<Library> queryLibraryPage(QueryArticle query, PageModel<Library> pageModel) {
        return libraryDao.queryLibraryPage(query, pageModel);
    }

    @Override
    public void updateDownloadNum(Map<String, String> map) {
        libraryDao.updateDownloadNum(map);
    }

    @Override
    public int queryAllLibraryCount() {
        return (int) libraryDao.queryAllLibraryCount();
    }

    public LibraryDao getLibraryDao() {
        return libraryDao;
    }

    public void setLibraryDao(LibraryDao libraryDao) {
        this.libraryDao = libraryDao;
    }
}
