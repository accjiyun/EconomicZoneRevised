package cn.accjiyun.service.impl;

import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.dao.ArticleContentDao;
import cn.accjiyun.dao.ArticleDao;
import cn.accjiyun.model.article.Article;
import cn.accjiyun.model.article.ArticleContent;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2016/11/16.
 */
@Service("articleService")
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleDao articleDao;

    @Autowired
    private ArticleContentDao articleContentDao;

    public int createArticle(Article article) {
        article.getArticleContent().setArticle(article);
        articleContentDao.addArticleContent(article.getArticleContent());
        return articleDao.createArticle(article);
    }

    public void addArticleContent(ArticleContent content) {
        articleContentDao.addArticleContent(content);
    }

    public void updateArticle(Article article) {
        articleDao.updateArticle(article);
    }

    public void updateArticleContent(ArticleContent content) {
        articleContentDao.updateArticleContent(content);
    }

    /**
     * 删除文章
     *
     * @param articleIds 文章ID数组
     */
    @Override
    public void deleteArticleByIds(String[] articleIds) {
        if (articleIds != null && articleIds.length > 0) {
            articleDao.deleteArticleByIds(articleIds);
        }
    }

    public Article queryArticleById(int articleId) {
        return articleDao.queryArticleById(articleId);
    }

    public String queryArticleContentByArticleId(int articleId) {
        return articleContentDao.queryArticleContentByArticleId(articleId);
    }

    /**
     * 分页查询文章列表
     */
    public List<Article> queryArticlePage(QueryArticle query, PageModel<Article> pageModel) {
        return articleDao.queryArticlePage(query, pageModel);
    }

    /**
     * 修改累加文章点击量
     *
     * @param map
     */
    @Override
    public void updateArticleNum(Map<String, String> map) {
        articleDao.updateArticleNum(map);
    }

    /**
     * 获取所有文章总记录数
     *
     * @return 总记录数
     */
    public int queryAllArticleCount() {
        return articleDao.queryAllArticleCount();
    }
}
