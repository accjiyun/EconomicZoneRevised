package cn.accjiyun.service.impl;

import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.dao.WebsiteLogoDao;
import cn.accjiyun.model.website.WebsiteLogo;
import cn.accjiyun.service.WebsiteLogoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiyun on 2017/2/10.
 */
@Service("websiteLogoService")
public class WebsiteLogoServiceImpl implements WebsiteLogoService {

    @Autowired
    private WebsiteLogoDao websiteLogoDao;

    @Override
    public List<WebsiteLogo> queryAllWebsiteLogo() {
        int count = (int) websiteLogoDao.getCount();
        PageModel<WebsiteLogo> pageModel = new PageModel<>();
        pageModel.setPageSize(count);
        return websiteLogoDao.queryWebsiteLogoPage(pageModel);
    }

    @Override
    public WebsiteLogo getWebsiteLogo() {
        return queryAllWebsiteLogo().get(0);
    }

    @Override
    public void setWebsiteLogo(WebsiteLogo websiteLogo) {
        websiteLogoDao.updateWebsiteLogo(websiteLogo);
    }
}
