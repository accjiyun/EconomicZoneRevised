package cn.accjiyun.model.article;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2016/11/12.
 */
@Entity
@Table(name = "article_content", schema = "economic_zone_db", catalog = "")
public class ArticleContent implements Serializable {
    /**
     * 文章ID
     */
    private int articleId;
    /**
     * 文章对应的内容
     */
    private String content;
    /**
     * 文章表
     */
    private Article article;

    @Id
    @Column(name = "ARTICLE_ID", nullable = false)
    @GenericGenerator(name = "foreignKey", strategy = "foreign",
            parameters = {@Parameter(name = "property", value = "article")})
    @GeneratedValue(generator = "foreignKey")
    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    @Basic
    @Column(name = "CONTENT", nullable = true, length = -1)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleContent that = (ArticleContent) o;

        if (articleId != that.articleId) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = articleId;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }
}
