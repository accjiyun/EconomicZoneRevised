package cn.accjiyun.model.article;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by jiyun on 2016/11/12.
 */
@Entity
@Table(name = "article", schema = "economic_zone_db")
public class Article implements Serializable {
    /**
     * 文章ID
     */
    private int articleId;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 关键词
     */
    private String keyWord;
    /**
     * 文章类型1:新闻速递,2:学术活动,3:通知公告,4：信息简报
     **/
    private int articleType;
    /**
     * 创建时间
     */
    private Timestamp createTime;
    /**
     * 发布时间
     */
    private Timestamp publishTime;
    /**
     * 发表状态：0为未发布，1为发布
     */
    private Integer status;
    /**
     * 文章图片URL
     */
    private String imageUrl;
    /**
     * 文章点击量
     */
    private Integer clickNum;
    /**
     * 排序值
     */
    private Integer sort;
    /**
     * 文章内容表
     */
    private ArticleContent articleContent;

    @Id
    @Column(name = "ARTICLE_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    public ArticleContent getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(ArticleContent articleContent) {
        this.articleContent = articleContent;
    }

    @Basic
    @Column(name = "ARTICLE_TYPE", nullable = false)
    public int getArticleType() {
        return articleType;
    }

    public void setArticleType(int articleType) {
        this.articleType = articleType;
    }

    @Basic
    @Column(name = "TITLE", nullable = true, length = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "KEY_WORD", nullable = true, length = 50)
    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    @Basic
    @Column(name = "CREATE_TIME", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "PUBLISH_TIME", nullable = true)
    public Timestamp getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Timestamp publishTime) {
        this.publishTime = publishTime;
    }

    @Basic
    @Column(name = "STATUS", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer stuts) {
        this.status = stuts;
    }

    @Basic
    @Column(name = "IMAGE_URL", nullable = true, length = 255)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Basic
    @Column(name = "CLICK_NUM", nullable = true)
    public Integer getClickNum() {
        return clickNum;
    }

    public void setClickNum(Integer clickNum) {
        this.clickNum = clickNum;
    }

    @Basic
    @Column(name = "SORT", nullable = true)
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        if (articleId != article.articleId) return false;
        if (articleType != article.articleType) return false;
        if (title != null ? !title.equals(article.title) : article.title != null) return false;
        if (createTime != null ? !createTime.equals(article.createTime) : article.createTime != null) return false;
        if (publishTime != null ? !publishTime.equals(article.publishTime) : article.publishTime != null) return false;
        if (status != null ? !status.equals(article.status) : article.status != null) return false;
        if (imageUrl != null ? !imageUrl.equals(article.imageUrl) : article.imageUrl != null) return false;
        if (clickNum != null ? !clickNum.equals(article.clickNum) : article.clickNum != null) return false;
        if (sort != null ? !sort.equals(article.sort) : article.sort != null) return false;
        if (keyWord != null ? !keyWord.equals(article.keyWord) : article.keyWord != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = articleId;
        result = 31 * result + (int) articleType;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (publishTime != null ? publishTime.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + (clickNum != null ? clickNum.hashCode() : 0);
        result = 31 * result + (sort != null ? sort.hashCode() : 0);
        result = 31 * result + (keyWord != null ? keyWord.hashCode() : 0);
        return result;
    }
}
