package cn.accjiyun.model.library;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by jiyun on 2017/2/10.
 */
@Entity
public class Library implements Serializable {
    /**
     * 智库ID
     */
    private int libraryId;
    /**
     * 智库类型1:智库专刊,2:蓝皮书
     */
    private Integer libraryType;
    /**
     * 智库标题
     */
    private String title;
    /**
     * 智库图片URL
     */
    private String imageUrl;
    /**
     * 智库创建时间
     */
    private Timestamp createTime;
    /**
     * 智库发布时间
     */
    private Timestamp publishTime;
    /**
     * 智库文件URL
     */
    private String libraryUrl;
    /**
     * 摘要
     **/
    private String summary;
    /**
     * 下载数
     **/
    private Integer downloadNum;

    @Id
    @Column(name = "LIBRARY_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(int libraryId) {
        this.libraryId = libraryId;
    }

    @Basic
    @Column(name = "LIBRARY_TYPE")
    public Integer getLibraryType() {
        return libraryType;
    }

    public void setLibraryType(Integer libraryType) {
        this.libraryType = libraryType;
    }

    @Basic
    @Column(name = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "SUMMARY")
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Basic
    @Column(name = "IMAGE_URL")
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Basic
    @Column(name = "CREATE_TIME")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "PUBLISH_TIME", nullable = true)
    public Timestamp getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Timestamp publishTime) {
        this.publishTime = publishTime;
    }

    @Basic
    @Column(name = "LIBRARY_URL")
    public String getLibraryUrl() {
        return libraryUrl;
    }

    public void setLibraryUrl(String libraryUrl) {
        this.libraryUrl = libraryUrl;
    }

    @Basic
    @Column(name = "DOWNLOAD_NUM")
    public Integer getDownloadNum() {
        return downloadNum;
    }

    public void setDownloadNum(Integer downloadNum) {
        this.downloadNum = downloadNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Library library = (Library) o;

        if (libraryId != library.libraryId) return false;
        if (libraryType != null ? !libraryType.equals(library.libraryType) : library.libraryType != null) return false;
        if (title != null ? !title.equals(library.title) : library.title != null) return false;
        if (summary != null ? !summary.equals(library.summary) : library.summary != null) return false;
        if (imageUrl != null ? !imageUrl.equals(library.imageUrl) : library.imageUrl != null) return false;
        if (createTime != null ? !createTime.equals(library.createTime) : library.createTime != null) return false;
        if (publishTime != null ? !publishTime.equals(library.publishTime) : library.publishTime != null) return false;
        if (libraryUrl != null ? !libraryUrl.equals(library.libraryUrl) : library.libraryUrl != null) return false;
        if (downloadNum != null ? !downloadNum.equals(library.downloadNum) : library.downloadNum != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = libraryId;
        result = 31 * result + (libraryType != null ? libraryType.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (summary != null ? summary.hashCode() : 0);
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (publishTime != null ? publishTime.hashCode() : 0);
        result = 31 * result + (libraryUrl != null ? libraryUrl.hashCode() : 0);
        result = 31 * result + (downloadNum != null ? downloadNum.hashCode() : 0);
        return result;
    }
}
