package cn.accjiyun.model.website;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2016/11/12.
 */
@Entity
@Table(name = "website_logo", schema = "economic_zone_db", catalog = "")
public class WebsiteLogo implements Serializable {
    /**
     * 网站概要图片ID
     */
    private int imageId;
    /**
     * 网站概要图片URL
     */
    private String imageUrl;

    @Id
    @Column(name = "IMAGE_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    @Basic
    @Column(name = "IMAGE_URL", nullable = true, length = 255)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WebsiteLogo that = (WebsiteLogo) o;

        if (imageId != that.imageId) return false;
        if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = imageId;
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        return result;
    }
}
