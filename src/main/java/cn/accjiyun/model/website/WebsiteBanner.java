package cn.accjiyun.model.website;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/2/11.
 */
@Entity
@Table(name = "website_banner", schema = "economic_zone_db", catalog = "")
public class WebsiteBanner implements Serializable {
    /**
     * 轮播图ID
     */
    private int bannerId;
    /**
     * 轮播图URL
     */
    private String imageUrl;
    /**
     * 排序值
     */
    private Integer sort;
    /**
     * 文章URL
     */
    private String articleUrl;
    /**
     * 轮播标题
     */
    private String title;

    @Id
    @Column(name = "BANNER_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getBannerId() {
        return bannerId;
    }

    public void setBannerId(int bannerId) {
        this.bannerId = bannerId;
    }

    @Basic
    @Column(name = "BANNER_IMAGE_URL", nullable = true, length = 255)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Basic
    @Column(name = "SORT", nullable = true)
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Basic
    @Column(name = "ARTICLE_URL", nullable = true, length = 255)
    public String getArticleUrl() {
        return articleUrl;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    @Basic
    @Column(name = "TITLE", nullable = true, length = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WebsiteBanner that = (WebsiteBanner) o;

        if (bannerId != that.bannerId) return false;
        if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null)
            return false;
        if (sort != null ? !sort.equals(that.sort) : that.sort != null) return false;
        if (articleUrl != null ? !articleUrl.equals(that.articleUrl) : that.articleUrl != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = bannerId;
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + (sort != null ? sort.hashCode() : 0);
        result = 31 * result + (articleUrl != null ? articleUrl.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }
}
