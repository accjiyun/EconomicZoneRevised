package cn.accjiyun.model.system;

import java.io.Serializable;

/**
 * 后台用户 查询辅助类
 * Created by jiyun on 2016/11/13.
 */
public class QuerySysUser implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer roleId;
    private String keyWord;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
}
