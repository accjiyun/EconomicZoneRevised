package cn.accjiyun.model.system;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by jiyun on 2017/4/27.
 */
@Entity
@Table(name = "login_log", schema = "economic_zone_db", catalog = "")
public class LoginLog {
    private int loginId;
    private Integer userId;
    private Timestamp loginTime;
    private String ipAddress;

    @Id
    @Column(name = "login_id", nullable = false)
    public int getLoginId() {
        return loginId;
    }

    public void setLoginId(int loginId) {
        this.loginId = loginId;
    }

    @Basic
    @Column(name = "user_id", nullable = true)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "login_time", nullable = true)
    public Timestamp getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Timestamp loginTime) {
        this.loginTime = loginTime;
    }

    @Basic
    @Column(name = "ip_address", nullable = true, length = 20)
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoginLog loginLog = (LoginLog) o;

        if (loginId != loginLog.loginId) return false;
        if (userId != null ? !userId.equals(loginLog.userId) : loginLog.userId != null) return false;
        if (loginTime != null ? !loginTime.equals(loginLog.loginTime) : loginLog.loginTime != null) return false;
        if (ipAddress != null ? !ipAddress.equals(loginLog.ipAddress) : loginLog.ipAddress != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = loginId;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (loginTime != null ? loginTime.hashCode() : 0);
        result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
        return result;
    }
}
