package cn.accjiyun.model.system;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by jiyun on 2016/12/4.
 */
@Entity
@Table(name = "sys_user", schema = "economic_zone_db")
public class SysUser implements Serializable {
    /**
     * 用户ID
     */
    private int userId;
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 登陆密码
     */
    private String loginPwd;
    /**
     * 所属角色ID
     */
    private Integer roleId;
    /**
     * 状态0:正常,1:冻结
     */
    private Integer status;
    /**
     * 最后登录时间
     */
    private Timestamp lastLoginTime;
    /**
     * 最后登录IP
     */
    private String lastLoginIp;
    /**
     * 创建时间
     */
    private Timestamp createTime;
    /**
     * 邮件地址
     */
    private String email;

    @Id
    @Column(name = "USER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "LOGIN_NAME")
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Basic
    @Column(name = "LOGIN_PWD")
    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    @Basic
    @Column(name = "ROLE_ID")
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "STATUS")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer staus) {
        this.status = staus;
    }

    @Basic
    @Column(name = "LAST_LOGIN_TIME")
    public Timestamp getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Timestamp lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    @Basic
    @Column(name = "LAST_LOGIN_IP")
    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    @Basic
    @Column(name = "CREATE_TIME")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "EMAIL")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysUser sysUser = (SysUser) o;

        if (userId != sysUser.userId) return false;
        if (loginName != null ? !loginName.equals(sysUser.loginName) : sysUser.loginName != null) return false;
        if (loginPwd != null ? !loginPwd.equals(sysUser.loginPwd) : sysUser.loginPwd != null) return false;
        if (roleId != null ? !roleId.equals(sysUser.roleId) : sysUser.roleId != null) return false;
        if (status != null ? !status.equals(sysUser.status) : sysUser.status != null) return false;
        if (lastLoginTime != null ? !lastLoginTime.equals(sysUser.lastLoginTime) : sysUser.lastLoginTime != null)
            return false;
        if (lastLoginIp != null ? !lastLoginIp.equals(sysUser.lastLoginIp) : sysUser.lastLoginIp != null) return false;
        if (createTime != null ? !createTime.equals(sysUser.createTime) : sysUser.createTime != null) return false;
        if (email != null ? !email.equals(sysUser.email) : sysUser.email != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (loginName != null ? loginName.hashCode() : 0);
        result = 31 * result + (loginPwd != null ? loginPwd.hashCode() : 0);
        result = 31 * result + (roleId != null ? roleId.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (lastLoginTime != null ? lastLoginTime.hashCode() : 0);
        result = 31 * result + (lastLoginIp != null ? lastLoginIp.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
