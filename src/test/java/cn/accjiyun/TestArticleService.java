package cn.accjiyun;

import cn.accjiyun.core.entity.PageModel;
import cn.accjiyun.model.article.Article;
import cn.accjiyun.model.article.ArticleContent;
import cn.accjiyun.model.article.QueryArticle;
import cn.accjiyun.service.ArticleService;
import com.alibaba.fastjson.JSON;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by jiyun on 2017/1/28.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring.xml",
        "classpath:applicationContext.xml"})
public class TestArticleService {
    private static final Logger LOGGER = Logger
            .getLogger(TestArticleService.class);

    @Autowired
    private ArticleService articleService;

    @Test
    public void queryById() {
        Article article = articleService.queryArticleById(2);
        LOGGER.info(JSON.toJSONString(article.getTitle()
                + article.getArticleContent().getContent()));
    }

    @Test
    public void queryArticlePage(){
        QueryArticle queryArticle = new QueryArticle();
        queryArticle.setType(1);
        queryArticle.setQueryKey("总书记");
        PageModel<Article> model = new PageModel<>();
        model.setPageSize(10);
        List<Article> articles = articleService.queryArticlePage(queryArticle, model);
        for (Article article : articles) {
            LOGGER.info(JSON.toJSONString(article.getTitle()
                    + article.getArticleContent().getContent()));
        }
    }

    @Test
    public void saveArticle() {
        Article article = new Article();
        ArticleContent articleContent = new ArticleContent();
        article.setArticleContent(articleContent);
        articleContent.setArticle(article);
        article.setTitle("Title");
        article.setArticleType(1);
        article.setCreateTime(new Timestamp(new Date().getTime()));
        articleService.createArticle(article);
    }

}
