package cn.accjiyun;

import cn.accjiyun.model.system.SysUser;
import cn.accjiyun.service.SysUserService;
import com.alibaba.fastjson.JSON;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by jiyun on 2017/2/11.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring.xml",
        "classpath:applicationContext.xml"})
public class TestUserService {
    private static final Logger LOGGER = Logger
            .getLogger(TestUserService.class);

    @Autowired
    private SysUserService sysUserService;

    @Test
    public void login() {
        SysUser sysUser = sysUserService.querySysUserByUserId(2);
        LOGGER.info(JSON.toJSONString(sysUser.getLoginName()));
    }
}
